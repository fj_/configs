#!/usr/bin/env python2.7
import sys, os, tempfile
import doctest, unittest
import resub
import shlex
from mockFS import MockFS
import mock_stdio

def set_pdb_hook():
    def info(type, value, tb):
        import traceback, pdb
        traceback.print_exception(type, value, tb)
        pdb.pm()
    sys.excepthook = info
set_pdb_hook()

fs = MockFS({
        'no_eol':'abc1',
        'eol':'abc2\n',
        'preview1': 'line1\nline2\nline3',
        'preview2': 'linep1\nlinep2\nlinep3\n',
        'preview3': 'line1\n\nline3\n',
        'preview4': 'line1\n  \nline3\n',
        'subdir': {
                'subdir1':{
                        'f' : 'someabcstring\n'
                        },
                },
        })
fs.install(resub)

class TestResub(unittest.TestCase):
    def cmd(self, cmdline, diff = {}, stdin = '', stdout = mock_stdio.DevNull, stderr = None):
        with fs.test(self, diff), mock_stdio.test(self, stdin, stdout, stderr):
            resub.main(shlex.split(cmdline))
            
    # general flags and file discovery tests
    def test_flags_preview_incompatible_with_inplace(self):
        with self.assertRaises(SystemExit):
            self.cmd(r'-i -p abc def eol', stderr = mock_stdio.DevNull)
    def test_flags_inplace_incompatible_with_reading_from_stdin(self):
        with self.assertRaises(SystemExit):
            self.cmd(r'-i abc def', stderr = mock_stdio.DevNull)

    def test_flags_preview_incompatible_with_multiline(self):
        with self.assertRaises(SystemExit):
            self.cmd(r'-pm abc def eol', stderr = mock_stdio.DevNull)

    def test_missing_file(self):
        with self.assertRaisesRegexp(AssertionError, 'match any files|doesn\'t exist'):
            self.cmd(r'abc def missing_file', stdout = '')

    def test_dir(self):
        with self.assertRaisesRegexp(AssertionError, 'only director'):
            self.cmd(r'-p abc def subdir', stdout = '')
            
    @unittest.skipIf(os.name != 'nt', 'requires Windows')
    def test_missing_file_pattern(self):
        with self.assertRaisesRegexp(AssertionError, 'match any files'):
            self.cmd(r'abc def *missing_file*', stdout = '')

    @unittest.skipIf(os.name != 'nt', 'requires Windows')
    def test_subdir_pattern(self):
        self.cmd(r'-i abc def */*/f', {'subdir/subdir1/f':'somedefstring\n'})

    def test_backup(self):
        self.cmd(r'-b bak abc def no_eol',
                {'no_eol' : 'def1\n', 'no_eol.bak' : 'abc1'},
                stdout = 'no_eol (1 substitutions)\n')

    def test_backup_deep(self):
        self.cmd(r'-b .lol abc def subdir/subdir1/f',
                {'subdir/subdir1/f':'somedefstring\n', 'subdir/subdir1/f..lol':'someabcstring\n'})

    # preview mode
    
    def test_preview_empty(self):
        self.cmd(r'-p abc def preview1 preview2', stdout = '')

    def test_preview_empty_no_matches(self):
        self.cmd(r'-p -n abc def preview1 preview2', stdout = 'preview1 (0 substitutions)\npreview2 (0 substitutions)\n')

    def test_preview_once(self):
        self.cmd(r'-p line2 def preview1 preview2', stdout = 'preview1\nline 2:\n- line2\n+ def\n(1 substitutions)\n')

    def test_preview_once_no_matches(self):
        self.cmd(r'-pn line2 def preview1 preview2', stdout = 'preview1\nline 2:\n- line2\n+ def\n(1 substitutions)\npreview2 (0 substitutions)\n')

    def test_preview_last(self):
        self.cmd(r'-p line3 def preview1', stdout = 'preview1\nline 3:\n- line3\n+ def\n(1 substitutions)\n')

    def test_preview_clear_line(self):
        self.cmd(r'-p line2 "" preview1', stdout = 'preview1\nline 2:\n- line2\n+ \n(1 substitutions)\n')

    def test_preview_clear_last_line_noeol(self):
        # There's no "\n".
        self.cmd(r'-p line3 "" preview1', stdout = 'preview1\nline 3:\n- line3\n+ \n(1 substitutions)\n')

    def test_preview_clear_last_line_proper(self):
        self.cmd(r'-p linep3 "" preview2', stdout = 'preview2\nline 3:\n- linep3\n+ \n(1 substitutions)\n')

    def test_preview_clear_spaces(self):
        self.cmd(r'-p "^\s+" "" preview4', stdout = 'preview4\nline 2:\n-   \n+ \n(1 substitutions)\n')

    def test_preview_insert_empty_lines(self):
        self.cmd(r'-p "^\s+" "\n" preview4', stdout = 'preview4\nline 2:\n-   \n+ \n+ \n(1 substitutions)\n')

    def test_preview_insert_lines(self):
        self.cmd(r'-p "^\s+" "l1\nl2\nl3" preview4', stdout = 'preview4\nline 2:\n-   \n+ l1\n+ l2\n+ l3\n(1 substitutions)\n')

    def test_preview_insert_lines2(self):
        self.cmd(r'-p "^\s+" "\nl1\nl2\nl3\n" preview4', stdout = 'preview4\nline 2:\n-   \n+ \n+ l1\n+ l2\n+ l3\n+ \n(1 substitutions)\n')

    def test_preview_ineffectual_substitutions1(self):
        self.cmd(r'-p line2 line2 preview1', stdout = '')

    def test_preview_ineffectual_substitutions2(self):
        self.cmd(r'-pn line2 line2 preview1', stdout = 'preview1 (0 substitutions)\n')

    def test_preview_ineffectual_substitutions3(self):
        self.cmd(r'-pn line\\d line2 preview1', stdout = 'preview1\nline 1:\n- line1\n+ line2\nline 3:\n- line3\n+ line2\n(2 substitutions)\n')
        
    # in-place mode
        
    def test_inplace_eol(self):
        self.cmd(
                r'-i abc def eol',
                {'eol' : 'def2\n'},
                stdout = 'eol (1 substitutions)\n')

    def test_inplace_no_eol(self):
        self.cmd(r'-i abc def no_eol',
                {'no_eol' : 'def1\n'},
                stdout = 'no_eol (1 substitutions)\n')
    
    # also checks that multilines are preserved    
    def test_inplace_empty(self):
        self.cmd(r'-i abc def preview1 preview2', stdout = '')

    def test_inplace_empty_no_matches(self):
        self.cmd(r'-i -n abc def preview1 preview2', 
                stdout = 'preview1 (0 substitutions)\npreview2 (0 substitutions)\n')

    def test_inplace_once(self):
        self.cmd(r'-i line2 def preview1 preview2',
                { 'preview1': 'line1\ndef\nline3\n' },
                stdout='preview1 (1 substitutions)\n')

    def test_inplace_once_no_matches(self):
        self.cmd(r'-in line2 def preview1 preview2',
                { 'preview1': 'line1\ndef\nline3\n' },
                stdout='preview1 (1 substitutions)\npreview2 (0 substitutions)\n')

    def test_inplace_last(self):
        self.cmd(r'-in line3 def preview1',
                { 'preview1': 'line1\nline2\ndef\n' },
                stdout='preview1 (1 substitutions)\n')

    def test_inplace_clear_line(self):
        self.cmd(r'-in line2 "" preview1',
                { 'preview1': 'line1\n\nline3\n' },
                stdout='preview1 (1 substitutions)\n')

    def test_inplace_clear_last_line_noeol(self):
        # There's no "\n".
        self.cmd(r'-i line3 "" preview1',
                { 'preview1': 'line1\nline2\n\n' },
                stdout='preview1 (1 substitutions)\n')

    def test_inplace_clear_last_line_proper(self):
        self.cmd(r'-i linep3 "" preview2',
                { 'preview2': 'linep1\nlinep2\n\n' },
                stdout='preview2 (1 substitutions)\n')
        
    def test_inplace_clear_spaces(self):
        self.cmd(r'-i "^\s+" "" preview4',
                { 'preview4': 'line1\n\nline3\n' },
                stdout='preview4 (1 substitutions)\n')

    def test_inplace_insert_empty_lines(self):
        self.cmd(r'-i "^\s+" "\n" preview4',
                { 'preview4': 'line1\n\n\nline3\n' },
                stdout='preview4 (1 substitutions)\n')

    def test_inplace_insert_lines(self):
        self.cmd(r'-i "^\s+" "l1\nl2\nl3" preview4',
                { 'preview4': 'line1\nl1\nl2\nl3\nline3\n' },
                stdout='preview4 (1 substitutions)\n')

    def test_inplace_insert_lines2(self):
        self.cmd(r'-i "^\s+" "\nl1\nl2\nl3\n" preview4',
                { 'preview4': 'line1\n\nl1\nl2\nl3\n\nline3\n' },
                stdout='preview4 (1 substitutions)\n')
        
    def test_inplace_ineffectual_substitutions1(self):
        self.cmd(r'-i line2 line2 preview1', stdout = '')

    def test_inplace_ineffectual_substitutions2(self):
        self.cmd(r'-in line2 line2 preview1', stdout = 'preview1 (0 substitutions)\n')

    def test_inplace_ineffectual_substitutions3(self):
        self.cmd(r'-in line\\d line2 preview1', 
                { 'preview1': 'line2\nline2\nline2\n' },
                stdout = 'preview1 (2 substitutions)\n')

    # multiline
    def test_inplace_multiline_simple(self):
        self.cmd(r'-im "line1" "zzz" preview4',
                { 'preview4': 'zzz\n  \nline3\n' },
                stdout='preview4 (1 substitutions)\n')
        
    def test_inplace_multiline_simple_noeol(self):
        self.cmd(r'-im "line2" "zzz" preview1',
                { 'preview1': 'line1\nzzz\nline3\n' },
                stdout='preview1 (1 substitutions)\n')

    def test_inplace_multiline_remove_spaces(self):
        self.cmd(r'-im "^\s+$" "" preview4',
                { 'preview4': 'line1\n\nline3\n' },
                stdout='preview4 (1 substitutions)\n')
    
    def test_inplace_multiline_delete_space_lines(self):
        self.cmd(r'-im "^\s+\n" "" preview4',
                { 'preview4': 'line1\nline3\n' },
                stdout='preview4 (1 substitutions)\n')

    def test_inplace_multiline_delete_all_whitespace(self):
        self.cmd(r'-im "\s+" "" preview4',
                { 'preview4': 'line1line3\n' },
                stdout='preview4 (2 substitutions)\n')
    
    def test_inplace_multiline_clear_all_lines(self):
        self.cmd(r'-im ".+" "" preview4',
                { 'preview4': '\n\n\n' },
                stdout='preview4 (3 substitutions)\n')

    def test_inplace_multiline_delete_totally_everything(self):
        self.cmd(r'-imS ".+" "" preview4',
                { 'preview4': '' },
                stdout='preview4 (1 substitutions)\n')

    # also checks that multilines are preserved    
    def test_inplace_multiline_empty(self):
        self.cmd(r'-im abc def preview1 preview2', stdout = '')

    def test_inplace_multiline_empty_no_matches(self):
        self.cmd(r'-imn abc def preview1 preview2', 
                stdout = 'preview1 (0 substitutions)\npreview2 (0 substitutions)\n')

    def test_inplace_multiline_once(self):
        self.cmd(r'-im line2 def preview1 preview2',
                { 'preview1': 'line1\ndef\nline3\n' },
                stdout='preview1 (1 substitutions)\n')

    def test_inplace_multiline_once_no_matches(self):
        self.cmd(r'-imn line2 def preview1 preview2',
                { 'preview1': 'line1\ndef\nline3\n' },
                stdout='preview1 (1 substitutions)\npreview2 (0 substitutions)\n')

    
    def test_normal_multiline_delete_space_lines(self):
        self.cmd(r'-m "^\s+$\n" ""',
                stdin  = 'line1\n  \nline3\n',
                stdout = 'line1\nline3\n',
                stderr = '<stdin> (1 substitutions)\n')
        
    def test_normal_multiline_delete_space_lines_noeol(self):
        self.cmd(r'-m "^\s+$\n" ""',
                stdin  = 'line1\n  \nline3',
                stdout = 'line1\nline3\n',
                stderr = '<stdin> (1 substitutions)\n')
        
    # normal mode
    def test_normal_eol(self):
        self.cmd(
                r'abc def eol',
                stdout = 'def2\n',
                stderr = 'eol (1 substitutions)\n')

    def test_normal_no_eol(self):
        self.cmd(r'abc def no_eol',
                stdout = 'def1\n',
                stderr = 'no_eol (1 substitutions)\n')

    def test_normal_empty(self):
        self.cmd(r'abc def preview1 preview2', 
                stdout = 'line1\nline2\nline3\nlinep1\nlinep2\nlinep3\n',
                stderr = '')
        
    def test_normal_empty_no_matches(self):
        self.cmd(r'-n abc def preview1 preview2', 
                stdout = 'line1\nline2\nline3\nlinep1\nlinep2\nlinep3\n',
                stderr = 'preview1 (0 substitutions)\npreview2 (0 substitutions)\n')

    def test_normal_once(self):
        self.cmd(r'line2 def preview1 preview2',
                stdout = 'line1\ndef\nline3\nlinep1\nlinep2\nlinep3\n',
                stderr='preview1 (1 substitutions)\n')

    def test_normal_once_no_matches(self):
        self.cmd(r'-n line2 def preview1 preview2',
                stdout = 'line1\ndef\nline3\nlinep1\nlinep2\nlinep3\n',
                stderr = 'preview1 (1 substitutions)\npreview2 (0 substitutions)\n')
        
    # reading from stdin
    def test_stdin_empty(self):
        self.cmd(r'abc def',
                stdin  = '',
                stdout = '',
                stderr = '')

    def test_stdin_empty_no_matches(self):
        self.cmd(r'-n abc def',
                stdin  = '',
                stdout = '',
                stderr = '<stdin> (0 substitutions)\n')
        
    def test_stdin_no_eol(self):
        self.cmd(r'abc def',
                stdin  = 'abc1',
                stdout = 'def1\n',
                stderr = '<stdin> (1 substitutions)\n')
        
    def test_stdin_eol(self):
        self.cmd(
                r'abc def',
                stdin  = 'abc2\n',
                stdout = 'def2\n',
                stderr = '<stdin> (1 substitutions)\n')


    def test_stdin_not_found_eol(self):
        self.cmd(r'abc def', 
                stdin  = 'line1\nline2\nline3\n',
                stdout = 'line1\nline2\nline3\n',
                stderr = '')

    def test_stdin_not_found_no_eol(self):
        self.cmd(r'abc def', 
                stdin  = 'line1\nline2\nline3',
                stdout = 'line1\nline2\nline3\n',
                stderr = '')
        
    def test_stdin_not_found_no_eol_no_matches(self):
        self.cmd(r'-n abc def', 
                stdin  = 'line1\nline2\nline3',
                stdout = 'line1\nline2\nline3\n',
                stderr = '<stdin> (0 substitutions)\n')


def main():
    os.chdir(tempfile.gettempdir())
    s1 = doctest.DocTestSuite(resub)
    s2 = unittest.TestLoader().loadTestsFromTestCase(TestResub)
    r = unittest.TextTestRunner(verbosity=4)
    r.run(s1)
    r.run(s2)

if __name__ == '__main__':
    main()

