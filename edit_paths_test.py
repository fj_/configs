#/bin/env python3

import doctest, unittest
import edit_paths
import sys
import io

class TestCommands(unittest.TestCase):
    def runcmd(self, src, cmd, res):
        stored = sys.stdin, sys.stdout
        try:
            class BlackHole(object):
                def write(self, s): pass
            sys.stdout = BlackHole()

            def execute(cmd):
                es = edit_paths.ExecutionScope()
                es.testresult = None
                es.load_env_value = lambda: src
                es.store_env_value = lambda value: setattr(es, 'testresult', value)
                es.execute(edit_paths.parse_cmd(cmd))
                return es.testresult

            # single cmd
            sys.stdin = io.StringIO()
            self.assertEqual(res, execute(cmd))

            # interactive, committed
            sys.stdin = io.StringIO(cmd + '\ncommit\n')
            self.assertEqual(res, execute(''))

            # interactive, aborted
            sys.stdin = io.StringIO(cmd + '\nabort\n')
            self.assertEqual(None, execute(''))
        finally:
            sys.stdin, sys.stdout = stored


    def testadd(self):
        self.runcmd('', 'add abc', 'abc')
        self.runcmd('xxx;yyy', 'add abc', 'abc;xxx;yyy')
        self.runcmd('xxx;yyy', 'add 1', 'yyy;xxx')


    def testquoting(self):
        self.runcmd('', 'add "" "" xxx "" ""', 'xxx')
        self.runcmd('xxx;yyy', 'add " yyy ; ;"', 'yyy;xxx')
        self.runcmd('xxx;yyy', 'add " aaa ; ; bbb;aaa"', 'aaa;bbb;xxx;yyy')


    def testaddback(self):
        self.runcmd('', 'addback abc', 'abc')
        self.runcmd('xxx;yyy', 'addback abc', 'xxx;yyy;abc')
        self.runcmd('xxx;yyy', 'addback 0', 'yyy;xxx')


    def testremove(self):
        self.runcmd('x x x;y y y', 'remove "y y y"', 'x x x')
        self.runcmd('x x x;y y y', 'remove 0', 'y y y')
        self.runcmd('x x x;y y y', 'remove 1', 'x x x')


    def testreplace(self):
        self.runcmd('x x x;y y y', 'replace 1', 'y y y')
        self.runcmd('x x x;y y y', 'replace "x y z"', 'x y z')


if __name__ == '__main__':
    s1 = doctest.DocTestSuite(edit_paths)
    s2 = unittest.TestLoader().loadTestsFromTestCase(TestCommands)
    r = unittest.TextTestRunner(verbosity=4)
    r.run(s1)
    r.run(s2)
