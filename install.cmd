@echo off
REM On types of links: hardlinks are more or less unusable for the task.  You remove the file and do
REM "hg up", move the repository around, anything; the far-end hardlink gets unsynchronized,
REM silently.  Sadly, true symlinks to files did not appear until Vista, and not many programs
REM follow *.lnk, so I just rerun install whenever in doubt.  Also, there are softlinks to
REM directories (so called junctions), they kinda work, except that deleting a junction with
REM explorer on XP/2k3 means recursively deleting its contents or, even, better invisibly placing
REM them in the Recycle Bin and deleting at some unspecified moment in the future. The whole point
REM is that it's all under version control, but still, take care.
REM
REM mklink requires running from an elevated cmd prompt (and backwardly granting yourself permission
REM doesn't help if you're a member of Administrators).
@echo on

SET CURDIR=%~dp0
mklink /J %USERPROFILE%\vimfiles %CURDIR%_vim
mklink /J %USERPROFILE%\.vscode\extensions\theme-Solarized-light-my %CURDIR%theme-Solarized-light-my
mklink "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\default.ahk" "%CURDIR%default.ahk"
mklink "%USERPROFILE%\.ackrc" "%CURDIR%_ackrc"
cp "XWin Server x64.lnk" "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"

REM regedit /s Windows.reg
