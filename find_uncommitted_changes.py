#!/usr/bin/env python2.7
from __future__ import print_function, division

import sys, os
from os import path as os_path
from glob import glob
import re
from shellcall import sh, ShellReturnCodeException


def main():
    printbuf_full = []
    for dir in sorted(glob('*/')):
        assert os.path.isdir(dir)

        printbuf_local = []
        def p(s):
            printbuf_local.append(s)

        def plines(lines):
            LIMIT = 10
            length = len(lines)
            if length > LIMIT:
                lines = lines[:LIMIT - 1]
                lines.append('... and {} more'.format(length - len(lines)))
            p('\n'.join(lines))
            p('')

        p(dir)
        print(dir)

        git_config = os.path.join(dir, '.git', 'config')
        if not os.path.isfile(git_config):
            continue
        git = sh.git(C=dir).as_command()
        try:
            url = git.config('remote.origin.url').line()
        except ShellReturnCodeException as exc:
            if exc.returncode == 1:
                p('!!! Repository has no remote configured')
                p('')
            else:
                raise


        def get_modified_files():
            return git.status(porcelain=True, untracked_files='no').list()

        lines = get_modified_files()

        if lines:
            p('!!! Repository has local uncommitted changes')
            plines(lines)

        lines = git.log('--oneline', '--decorate', '--branches', '--not', '--remotes').list()
        if len(lines):
            p('!!! Repository has unpushed changes')
            plines(lines)

        lines = git.stash.list().list()
        if len(lines):
            p('!!! Repository has stashed changes')
            plines(lines)

        if len(printbuf_local) > 1:
            printbuf_full.extend(printbuf_local)
            if sys.stdout.isatty():
                # up one line and print red
                print('\x1b[A' + '\x1b[41m' + printbuf_local[0] + '\x1b[0m')

    if printbuf_full:
        print('\n==== CHANGES ====\n\n')
        print('\n'.join(printbuf_full))

if __name__ == '__main__':
    main()
