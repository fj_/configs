import re
import functools
#import sys

__all__ = ['prepare_nomagic_pattern', 'make_multi_replacer',
        'make_nomagic_replacer', 'fix_files',
        'indent', 'captured_indent', 'identifier', 'captured_identifier', 
        'integer', 'captured_integer', 
        'named_identifier', 'named_id_backreference']


def prepare_nomagic_pattern(pattern):
    r'''Convert a string with something resembling a programming language
    expression into a regex pattern that matches it, ignoring insignificant
    whitespace.

    First of all, every character in the expression is considered to be literal
    and you have to escape it get to the special functionality, like with the
    'nomagic' regex mode in Vim. For example, 's' matches letter s, '\s'
    matches whitespace, '+' matches plus sign, '\+' modifies previous atom to
    match one or more times.

    The basic conceptual model for dealing with whitespace is a very simple
    lexer which only understands 'identifiers' (made of alphanumerics and
    underscores) and single-character 'operators' (everything else), so that
    "==" is treated as two lexemes for example. Identifiers are separated from
    each other by \s+, operators are separated from other operators and from
    identifiers by \s*, after I've split the pattern into lexemes according to
    these rules all original whitespace is discarded, then lexemes are joined
    into a regex with separators inserted according to the same rules.

    A third lexeme class, 'control', are escaped single characters that are
    passed through to the regex engine, unescaped in the process if they do not
    belong to identifiers (that is, converted from my 'nomagic' to the standard
    re mode).

    Control characters are treated as operators during the parsing stage, but
    do not implicitly allow any whitespace around and between themselves, so
    since all whitespace is stripped at the parsing stage, regex parts of the
    pattern behave as if compiled with re.VERBOSE and '\s\*' or '\ \*' should
    be used to allow spaces explicitly.

    Also, '\b' (word boundary zero-width assertions) are inserted at the
    beginning and the end of the pattern if it begins/ends with an identifier
    character.

    Finally, sometimes you need to use ordinary regexes for large consecutive
    parts of the pattern, to cut on the amount of necessary escapes '\@' begins
    a literal section which lasts until the next '\@' and is passed to re
    completely unchanged (so make sure you compile with re.VERBOSE if you want
    to use spaces there).  This section is treated as a single control
    character in terms of surrounding whitespace.

    Notes: _everything_ except identifiers is escaped (or unescaped if was
    escaped), not just the nonalpha characters that re considers special. This
    way things like '\(\?P\<name\>zzz\)' (named capturing group) are translated
    correctly, with no extra '\s*' inserted. Also, character class
    specifications (like '[0-9]') don't need special treatment from the
    translator despite having a different list of magic characters.

    If you want to match from the beginning of a line or capture indentation,
    do not use '\s*', because it would capture newlines as well. Use
    r'\^ \[\ \t\]\*' or r'\@ ^[ \t]* \@'.

    >>> f = prepare_nomagic_pattern
    >>> f(r'asd')
    '\\basd\\b'

    >>> f(r'  asd  ')
    '\\basd\\b'

    >>> f(r'\ asd')
    '\\ asd\\b'

    >>> f(r'\  asd')
    '\\ asd\\b'

    >>> f(r'\^asd')
    '^asd\\b'

    >>> f(r'asd asd')
    '\\basd\\s+asd\\b'

    >>> f(r'asd  asd')
    '\\basd\\s+asd\\b'

    >>> f(r'asd \  asd')
    '\\basd\\ asd\\b'

    >>> f(r'a()')
    '\\ba\\s*\\(\\s*\\)'

    >>> f(r' a ( ) ')
    '\\ba\\s*\\(\\s*\\)'

    >>> f(r' \s\* \w\+ \s\+ ')
    '\\s*\\w+\\s+'

    >>> f(r' \@ \s* @\w+ \s+ \@ asd')
    ' \\s* @\\w+ \\s+ asd\\b'

    >>> f(r'')
    ''
    '''

    def generator():
        # FSM states
        s_start                 = 's_start'
        s_in_identifier         = 's_in_identifier'
        s_after_identifier      = 's_after_identifier'
        s_after_operator        = 's_after_operator'
        s_after_escape          = 's_after_escape'
        s_after_control         = 's_after_control'
        s_in_raw                = 's_in_raw'
        s_in_raw_after_escape   = 's_in_raw_after_escape'
        state = s_start

        for c in pattern:
            # special states
            if state is s_after_escape:
                if c == '@':
                    state = s_in_raw
                elif c.isalnum() or c == '_' or c == ' ':
                    # leave space escaped too, in case of re.VERBOSE
                    yield '\\' + c
                    state = s_after_control
                else:
                    yield c
                    state = s_after_control

            elif state is s_in_raw:
                if c == '\\':
                    state = s_in_raw_after_escape
                else:
                    yield c

            elif state is s_in_raw_after_escape:
                if c == '@':
                    state = s_after_control
                else:
                    yield '\\' + c
                    state = s_in_raw

            else: # usual parsing
                if c == '\\':
                    state = s_after_escape
                elif c.isspace():
                    if state is s_in_identifier:
                        state = s_after_identifier
                elif c.isalnum() or c == '_':
                    if state is s_start:
                        yield '\\b'
                    elif state is s_after_identifier:
                        yield '\\s+'
                    elif state is s_after_operator:
                        yield '\\s*'
                    yield c
                    state = s_in_identifier
                else:
                    # 'operator'
                    if state in (s_in_identifier, s_after_identifier, s_after_operator):
                        yield '\\s*'
                    yield '\\' + c
                    state = s_after_operator
        # terminating pseudo-state
        assert state is not s_after_escape, 'Unterminated escape sequence: %r' % pattern
        assert state not in (s_in_raw, s_in_raw_after_escape), 'Unterminated raw section (started by \\@): %r' % pattern
        if state in (s_in_identifier, s_after_identifier):
            yield '\\b'
    return ''.join(generator())


def int_tryparse(s):
    try:
        return int(s)
    except ValueError, TypeError:
        return None


def re_replacer(s):
    rx = re.compile(s)
    def outer_wrapper(f):
        @functools.wraps(f)
        def inner_wrapper(data):
            return rx.sub(f, data)
        return inner_wrapper
    return outer_wrapper


def iter_pairs(src):
    '''Unified access to dictionaries or lists of pairs

    >>> sorted(iter_pairs({1: '1', 2: '2', 3: '3'}))
    [(1, '1'), (2, '2'), (3, '3')]

    >>> sorted(iter_pairs([(1, '1'), (2, '2'), (3, '3')]))
    [(1, '1'), (2, '2'), (3, '3')]
    '''
    if hasattr(src, 'iteritems'):
        return src.iteritems()
    return iter(src)


# I need this group_reduce thing because Python has a limit of 100 capturing
# groups in regular expressions, so I want to automatically split the combined
# regex into several if necessary, which greatly complicated
# make_multi_replacer, while running in group_reduce makes it pretty
# straightforward
class _GroupReduceIterator(object):
    def __init__(self, first, rest):
        self.first = first
        self.rest = rest
        self.state = 0
        # 0 -- initial
        # 1 -- got item from first
        # 2 -- got item from rest
        # 3 -- exhausted
    def __iter__(self): return self
    def next(self): #@ReservedAssignment
        if self.state == 0:
            self.state = 1
            return self.first
        try:
            first = self.first = next(self.rest)
            self.state = 2
            return first
        except: # except all exceptions because that's how all generators work.
            self.state = 3
            raise


def group_reduce(f, enumerable):
    '''Splits a sequence into non-empty runs of items and reduces each run
    using the provided function. When the function returns, the last item it
    consumed is reused as the first item in the next run, unless it was the only
    item. The return value is yielded. The function is guaranteed that the
    iterator provided to it contains at least one value.

    >>> def split_ten(iterator):
    ...     total = next(iterator)
    ...     lst = [total]
    ...     for n in iterator:
    ...         total += n
    ...         if total >= 10: break
    ...         lst.append(n)
    ...     return lst
    ...
    >>> list(group_reduce(split_ten, [1, 7, 2, 11, 11, 1, 8]))
    [[1, 7], [2], [11], [11], [1, 8]]

    >>> list(group_reduce(lambda iterator: None, [1, 2, 3]))
    Traceback (most recent call last):
        ...
    AssertionError: Function failed to consume an item

    >>> list(group_reduce(lambda iterator: next(iterator), [1, 2, 3]))
    [1, 2, 3]
    '''
    rest = iter(enumerable)
    iterator = _GroupReduceIterator(next(rest), rest)
    del rest
    yield f(iterator)
    while 1:
        if iterator.state == 0:
            assert False, 'Function failed to consume an item'
        elif iterator.state == 1:
            # special-case the first item.
            next(iterator)
            iterator.state = 0
        elif iterator.state == 2:
            iterator.state = 0
        else:
            break
        yield f(iterator)


def collect_patterns_for_one_replacer(flags, iterator):
    # I add parentheses around each pattern which corresponds to the logical
    # group 0 for it, so these calculations are a bit complicated.
    total_groups = 1
    patterns = []
    replacers = []

    for counter, (pattern, compiled, replacement) in enumerate(iterator):
        groups_count = compiled.groups + 1
        if counter and groups_count + total_groups >= 100:
            break
        named_groups = set()
        def unique_name(s):
            return '_%d_%s' % (counter, s)

        re_paired_escapes = r'(?<!\\)((?:\\\\)*)' # creates a group for the stuff that needs to be inserted back!

        @re_replacer(re_paired_escapes + r'\(\?P<(\w+)>') # no closing parenthesis because I can't match it with a regex!
        def patch_named_groups(match):
            name = match.group(2)
            assert name not in named_groups, 'Group name not unique within a single pattern: %r' % name
            named_groups.add(name)
            return '%s(?P<%s>' % (match.group(1), unique_name(name))

        @re_replacer(re_paired_escapes + r'\(\?P=(\w+)\)')
        def patch_named_backreferences(match):
            name = match.group(2)
            assert name in named_groups, 'Unknown group name: %r' % name
            return '%s(?P=%s)' % (match.group(1), unique_name(name))

        @re_replacer(re_paired_escapes + r'\\g<(\w+)>')
        def patch_references(match):
            name = match.group(2)
            ref = int_tryparse(name)
            if ref is not None:
                assert 0 <= ref < groups_count, (
                        'pattern: %r, replacement: %r, ref: %r, groups_count: %r' %
                        (pattern, replacement, ref, groups_count))
                ref += total_groups
                name = str(ref)
            else:
                assert name in named_groups, 'Unknown group name: %r' % name
                name = unique_name(name)
            return '%s\\g<%s>' % (match.group(1), name)

        patched_pattern = patch_named_groups(pattern)
        patched_pattern = patch_named_backreferences(patched_pattern)
        patched_pattern = '(' + patched_pattern + ')'
        patterns.append(patched_pattern)

        # I have to use named arguments to capture variables in replacer
        if callable(replacement):
            def replacer(match,
                    compiled=compiled,
                    replacement=replacement):
                s = match.group(0)
                match2 = compiled.match(s)
                assert match2, 'Failed to match %r with pattern %r again.' % (
                        s, compiled.pattern)
                assert len(s) == match2.end() - match2.start(), (
                        'Failed to match the entire %r with pattern %r again (only matched %r).' % (
                        s, compiled.pattern, match2.group(0)))
                return replacement(match2)
        else:
            def replacer(match,
                    replacement=patch_references(replacement)):
                return match.expand(replacement)

        # I guess it's more efficient to insert a reference to this replacer
        # for each group of the corresponding match than to binary search for
        # it later.
        for _ in xrange(groups_count):
            replacers.append(replacer)
        total_groups += groups_count
    pattern = '|'.join(patterns)
    rx = re.compile(pattern, flags)
    def sub(match):
        return replacers[match.lastindex - 1](match)
    return lambda s: rx.sub(sub, s)


def make_multi_replacer(substitutions, flags=0, allow_multipass=False):
    r'''Construct a one-pass multi-substitution mapping.

    `substitutions` can be a dictionary or a list (or other iterable) of
    pattern-replacement pairs.

    Groups names are not required to be unique as I patch both names and
    indices. '\n' backreference syntax is ambigous and can backfire horribly as
    a result of patching indices, so group substitutions in templates must use
    the proper '\g<n>' syntax (named or by index) and group backreferences in
    patterns must use named groups '(?P=name)'.

    If replacement is a function, then the corresponding pattern is applied
    again to the matched substring to produce a proper match object. Therefore
    you should be careful with things like lookbehind assertions in the
    beginning of the pattern which could cause it to fail to match the second
    time.

    Uses re.MULTILINE | re.VERBOSE flags by default.

    Python re implementation doesn't allow using more than 100 capturing
    groups. Specify allow_multipass=True to automatically split substitutions
    into several passes. It is not enabled by default because it makes somewhat
    predictable behaviour of the engine in case of overlapping matches (that
    you might accidentally depend on) completely inscrutable.



    >>> replacer = make_multi_replacer([
    ... (r'abc', r'[R1 group0: \g<0>]'),
    ... (r'a(B+)c', r'[R2 group1: \g<1>]'),
    ... (r'(A+)(?:B+)(?P<name>C+)', r'[R3 group1: \g<1> group2: \g<2>]'),
    ... (r'c(?P<name>D+)(?P=name)e', r'[R4 namedgroup: \g<name>\g<name>\\g<name>\\\g<name>]'),
    ... (r'f(j*)', lambda match: r'[R5 lambda %s/%s]' % (match.group(0), match.group(1))),
    ... ])


    >>> replacer('AAABBBCCC abc aBBc cDDDDDDe fjj')
    '[R3 group1: AAA group2: CCC] [R1 group0: abc] [R2 group1: BB] [R4 namedgroup: DDDDDD\\g<name>\\DDD] [R5 lambda fjj/jj]'

    >>> replacer = make_multi_replacer([('(?<=a)bc', lambda match: '')])
    >>> replacer('abc')
    Traceback (most recent call last):
        ...
    AssertionError: Failed to match 'bc' with pattern '(?<=a)bc' again.

    >>> replacer = make_multi_replacer([('a(b(?=c))?', lambda match: '')])
    >>> replacer('abc')
    Traceback (most recent call last):
        ...
    AssertionError: Failed to match the entire 'ab' with pattern 'a(b(?=c))?' again (only matched 'a').
    '''

    items = ((pattern, re.compile(pattern, flags), replacement)
            for pattern, replacement in iter_pairs(substitutions))
    replacers = list(group_reduce(
        functools.partial(collect_patterns_for_one_replacer, flags), 
        items))
    assert len(replacers), 'Don\'t you want to give me some patterns?'
    if len(replacers) > 1:
        assert allow_multipass, ('Too much groups to fit in one regex, specify allow_multipass=True'
                ' to automatically split them into several replacers.')
        def replacer(s):
            for r in replacers:
                s = r(s)
            return s
        return replacer
    return replacers[0]





# utility functions for most frequent use cases

def make_nomagic_replacer(substitutions, flags=re.MULTILINE | re.VERBOSE, allow_multipass=False):
    '''Call make_multi_replacer with prepare_nomagic_pattern applied to each pattern.

    Uses `flags = re.MULTILINE | re.VERBOSE` by default'''
    return make_multi_replacer([(prepare_nomagic_pattern(k), v) for
        k, v in iter_pairs(substitutions)], flags, allow_multipass)


def str_diff(s1, s2):
    import difflib
    return ''.join(difflib.unified_diff(s1.splitlines(True), s2.splitlines(True)))


def fix_files(replacer, file_names, dry_run=False):
    modified_files = []
    for file_name in file_names:
        print file_name
        with open(file_name, 'r') as f:
            data = f.read()
        fixed = replacer(data)
        if fixed != data:
            fixed2 = replacer(fixed)
            if fixed2 != fixed:
                print 'Non-idempotent replacement cycle!'
                print str_diff(fixed, fixed2)
                assert False
            print str_diff(data, fixed)
            if not dry_run:
                with open(file_name, 'w') as f:
                    f.write(fixed)
            modified_files.append(file_name)
    return modified_files


indent = r'\@ ^[ \t]* \@'
captured_indent = r'\@ ^([ \t]*) (?!\s) \@'
identifier = r'\@ \s*\b \w+ \b\s* \@'
captured_identifier = r'\@ \s*\b (\w+) \b\s* \@'
integer = r'\@ \s*\b -?\s*\d+ \b\s* \@'
captured_integer = r'\@ \s*\b (-?\s*\d+) \b\s* \@'

def named_identifier(name):
    assert name.isalnum()
    return r'\@ \s*\b (?P<%s>\w+) \b\s* \@' % name

def named_id_backreference(name):
    assert name.isalnum()
    return r'\@ \s*\b (?P=%s) \b\s* \@' % name


if __name__ == '__main__':
    import doctest
    from unittest import TestSuite, TextTestRunner
    tests = TestSuite([doctest.DocTestSuite()])
    runner = TextTestRunner(verbosity=4)
    runner.run(tests)

