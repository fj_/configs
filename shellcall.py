'''It turns out that it's relatively easy to write a simple Python-to-shell interface with a lot of
convenience but not very scary magic. This one is inspired by sh and plumbum.

An example: `sh.git.branch(r=True).list()` returns a list of branches.

How the magic works:

First, you get a CommandBuilder, `sh` is an empty CommandBuilder root.

Accessing attributes on it returns further CommandBuilders with those commands appended to the
command-line, for easy sub-command chaining. Underscores in attribute names are converted to dashes.

When you call a CommandBuilder with some positional and keyword arguments, a CommandExecutor is
returned, with those arguments appended to its commandline.

CommandExecutor no longer allows sub-command chaining via attribute access magic, but doesn't run
the command yet. Instead you can append more arguments (in case you run into limitations of Python
argument passing rules) and finally execute the command in one of the several ways depending on what
you expect to be returned. All involved objects are immutable and return modifications in new
instances, so you can reuse partially built commands any way you want.

So the stages of the execution of the above example are:

>>> sh
CommandBuilder([])

>>> sh.git.branch
CommandBuilder(['git', 'branch'])

>>> sh.git.branch(r=True)
CommandExecutor(['git', 'branch', '-r'])

And then when you call ".list()" on it, it actually executes the command and returns the list of
lines it produced on the stdout.

Naturally you can compose some or all of the command line yourself:

>>> sh('git', 'branch', '-r')
CommandExecutor(['git', 'branch', '-r'])

When constructing CommandExecutor, positional arguments are appended first, then keyword arguments
are processed. Short (single-letter) and long option names are prepended with one or two dashes
respectively. Underscores in option names are replaced with dashes. If the option value is a string
then it's placed as the next argument, else if it's True or False the option is interpreted as a
flag, other values are not allowed. Long options with False value are prepended with '--no-'
instead, short options are not allowed to have False specified. Check the `construct_cmdline()`
function for examples.

Note that the order of keyword arguments is unspecified (so we sort them alphabetically). Also,
positional arguments always go before keyword arguments and are not checked for dashes in front (to
allow specifying switches as literal strings), so you might need to sanitize input like this:

>>> sh.git.rev_list(ancestry_path=True).positional('12345', '^12312')
CommandExecutor(['git', 'rev-list', '--ancestry-path', '--', '12345', '^12312'])

Or specify the positional arguments directly in the method call that executes the command (I'm using
`.get_cmdline()` here for illustration purposes):

>>> sh.git.rev_list(ancestry_path=True).get_cmdline('12345', '^12312')
"git rev-list --ancestry-path -- 12345 '^12312'"

But you should never do both or call `.positional()` more than once or call `.more_args()` after
`.positional()`, since you'll get extra positional argument delimiters ('--').
'''

import os, sys
import subprocess, threading
import functools
from pipes import quote as cmd_quote
from contextlib import contextmanager

# TODO: throw these instead of asserts
class ShellCommandException(Exception):
    pass


class ShellReturnCodeException(ShellCommandException):
    def __init__(self, args, returncode, stderr):
        import pipes
        Exception.__init__(self, 'Shell command {!r} returned {}'.format(
            ' '.join(pipes.quote(arg) for arg in args), returncode))
        # names consistent with subprocess
        self.returncode = returncode
        self.stderr = stderr


def tee_file(file, target):
    def worker():
        try:
            data = file.read(1024)
            if data == '':
                return
            target.write(data)
            os.write(w, data)
        finally:
            os.close(w)

    r, w = os.pipe()
    try:
        stdout_thread = threading.Thread(target=worker)
        stdout_thread.setDaemon(True)
        stdout_thread.start()
    except:
        os.close(r)
        os.close(w)
        raise
    return os.fdopen(r, 'rb')


def construct_cmdline(*args, **kwargs):
    '''
    >>> construct_cmdline('abc', '12 3', flag='xxx')
    ['abc', '12 3', '--flag=xxx']

    >>> construct_cmdline(flag_with_dashes='xxx')
    ['--flag-with-dashes=xxx']

    >>> construct_cmdline(flag=True)
    ['--flag']

    >>> construct_cmdline(flag=False)
    ['--no-flag']

    >>> construct_cmdline(flag=1)
    Traceback (most recent call last):
      ...
    AssertionError

    >>> construct_cmdline(f='xxx')
    ['-f', 'xxx']

    >>> construct_cmdline(f=True)
    ['-f']

    >>> construct_cmdline(f=False)
    Traceback (most recent call last):
      ...
    AssertionError
    '''
    result = list(args)
    def format_name(name, positive=True):
        name = name.replace('_', '-')
        assert not name.startswith('-')
        if len(name) == 1:
            assert positive
            return '-' + name
        else:
            return ('--' if positive else '--no-') + name

    for name, value in sorted(kwargs.iteritems()):
        if value is True or value is False:
            result.append(format_name(name, value))
        else:
            assert isinstance(value, basestring)
            name = format_name(name)
            if name.startswith('--'):
                result.append(name + '=' + value)
            else:
                result.append(name)
                result.append(value)
    return result


def add_positional_args(f):
    @functools.wraps(f)
    def wrapper(self, *args):
        executor = self.positional(*args) if args else self
        return f(executor)
    return wrapper

class CommandExecutor(object):
    def __init__(self, cmdline):
        self.cmdline = cmdline

    def more_args(self, *args, **kwargs):
        return CommandExecutor(self.cmdline + construct_cmdline(*args, **kwargs))

    def positional(self, *args):
        return CommandExecutor(self.cmdline + ['--'] + list(args))

    def as_command(self):
        return CommandBuilder(self.cmdline)

    @add_positional_args
    def retval(self):
        process = subprocess.Popen(self.cmdline)
        return process.wait()

    @add_positional_args
    def text(self):
        process = subprocess.Popen(self.cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        process.stderr = tee_file(process.stderr, sys.stderr)
        out, err = process.communicate()
        if process.returncode != 0:
            raise ShellReturnCodeException(self.cmdline, process.returncode, err)
        return out

    def list(self, *args):
        out = self.text(*args).split('\n')
        if len(out):
            assert out[-1] == ''
            out.pop()
        return out

    def line(self, *args):
        out = self.list(*args)
        assert len(out) == 1
        return out[0]

    @add_positional_args
    def get_cmdline(self):
        return ' '.join(cmd_quote(it) for it in self.cmdline)

    def __repr__(self):
        return 'CommandExecutor([' + ', '.join('{0!r}'.format(it) for it in self.cmdline) + '])'


class CommandBuilder(object):
    def __init__(self, cmdline=[]):
        self.cmdline = cmdline

    def __getattribute__(self, name):
        cmdline = object.__getattribute__(self, 'cmdline')
        return CommandBuilder(cmdline + [name.replace('_', '-')])

    def __call__(self, *args, **kwargs):
        cmdline = object.__getattribute__(self, 'cmdline')
        return CommandExecutor(cmdline + construct_cmdline(*args, **kwargs))

    def __repr__(self):
        cmdline = object.__getattribute__(self, 'cmdline')
        return 'CommandBuilder([' + ', '.join('{0!r}'.format(it) for it in cmdline) + '])'


sh = CommandBuilder() # default root object.


@contextmanager
def cwd(path):
    old_path = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(old_path)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
