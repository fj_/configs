#!/bin/bash
set -eux -o pipefail

git config --global core.editor vim
git config --global core.excludesfile ~/.gitignore
git config --global core.whitespace blank-at-eol,space-before-tab,-blank-at-eof

git config --global core.eol lf
git config --global core.autocrlf input
git config --global core.safecrlf warn

git config --global gui.fontui "-family helvetica -size 9 -weight bold -slant roman -underline 0 -overstrike 0"
git config --global gui.fontdiff "-family \"bitstream vera sans mono\" -size 10 -weight normal -slant roman -underline 0 -overstrike 0"

git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit"
git config --global alias.st "status"

git config --global receive.denyCurrentBranch updateInstead
git config --global push.default simple
git config --global merge.ff only
git config --global merge.conflictstyle diff3

git config --global color.diff auto
git config --global color.ui true


git config --global diff.compactionHeuristic true

git config --global rerere.enabled true

git config --global git-up.rebase.log-hook 'echo "changes on $1:"; git lg $1..$2; git diff --stat $1..$2'

