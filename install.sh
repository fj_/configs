#!/bin/bash

ln -sv $PWD/_bashrc ~/.bashrc
ln -sv $PWD/_bash_profile ~/.bash_profile
ln -sv $PWD/_inputrc ~/.inputrc
ln -sv $PWD/_ctags ~/.ctags
ln -sv $PWD/_pythonrc.py ~/.pythonrc.py
ln -sv $PWD/_ackrc ~/.ackrc
ln -sv $PWD/_ripgreprc ~/.ripgreprc

if [ -e ~/.vim ]; then
    echo "ln: creating symbolic link \``echo ~/.vim`': File exists" # I'm such a sly dog!
else
    ln -vs $PWD/_vim ~/.vim
fi

LOCAL=~/.local

mkdir -p $LOCAL/bin/startup-scripts
mkdir -p $LOCAL/lib/python2.6/site-packages
mkdir -p $LOCAL/lib/python2.7/site-packages


ln -sv $PWD/filter_python_exceptions $LOCAL/bin
ln -sv $PWD/gdbattach $LOCAL/bin

ln -sv $PWD/thirdparty/ack $LOCAL/bin
ln -sv $PWD/thirdparty/dtrx $LOCAL/bin
ln -sv $PWD/thirdparty/git-completion.bash $LOCAL/bin/startup-scripts/git-completion.bash
ln -sv $PWD/thirdparty/git-prompt.sh $LOCAL/bin/startup-scripts/git-prompt.sh

ln -sv $PWD/local_utils.py $LOCAL/lib/python2.7/site-packages
ln -sv $PWD/local_utils.py $LOCAL/lib/python2.6/site-packages

mkdir -p ~/.config/fontconfig
ln -sv $PWD/fonts.conf ~/.config/fontconfig
