from copy import deepcopy
from types import NoneType
import __builtin__

class Namespace(object):
    def __init__(self, **kwargs):
        self.__dict__ = kwargs
    def __str__(self):
        sep = '\n    '
        return 'Namespace:' + ''.join(
                '{}{} : {!r}'.format(sep, k, self.__dict__[k])
                for k in sorted(self.__dict__.iterkeys()))

def update_dict_unique(d, src):
    '''Raises KeyError if some keys are not unique or already in the dictionary.'''
    if isinstance(src, dict):
        src = src.iteritems()
    for k, v in src:
        if k in d:
            raise KeyError('duplicate key: {!r} with values {!r} and {!r}'.format(
                        k, d[k], v))
        d[k] = v

def make_dict_unique(lst):
    d = {}
    update_dict_unique(d, lst)
    return d

def get_public_exports(module):
    try:
        all = module.__all__
        return ((k, v) for k, v in module.__dict__.iteritems() if k in module.__all__)
    except:
        return module.__dict__.iteritems()


class HookedModuleDefinition(object):
    def __init__(self, target, hooks=[], safe_items=[], safe_classes={str, unicode, int, NoneType}):
        '''hooks: [(target, replacement)], safe_items: [target]'''
        self.target = target
        self.target_id = id(target)
        self.hooks = make_dict_unique((id(t), r) for t, r in hooks)
        self.safe_items = make_dict_unique((id(it), it) for it in safe_items)
        self.safe_classes = safe_classes
        # a little sanity check
        all_ids = {id(v) for _, v in get_public_exports(target)}
        for t, _ in hooks:
            assert id(t) in all_ids, 'Hook target {!r} doesn\'t belong to module {!r}'.format(t, target)
        for t in safe_items:
            assert id(t) in all_ids, 'Safe item {!r} doesn\'t belong to module {!r}'.format(t, target)

def install(module, builtin_overrides, *hook_defs):
    '''
    Returns a list [(key, original_value), ...] which can be used to restore
    original values, simply as `module.__dict__.update(original)`, if you want.
    
    Btw it is possible to actually override __builtins__ (sic!), but it's quite
    weird since it's cached in frame objects, which seems to be reset (usually)
    on any inter-module calls, but still... anyway, I would usually want to 
    only override `open` for example.
    '''
    restore = []
    mock_modules = make_dict_unique((id(hmd.target), Namespace()) for hmd in hook_defs)
    hooks = dict(mock_modules) # not a deep copy!!!
    safe_items = {}
    dangerous_items = {}
    for hmd in hook_defs:
        update_dict_unique(hooks, hmd.hooks)
        update_dict_unique(safe_items, hmd.safe_items)

    for hmd in hook_defs:
        ns = mock_modules[hmd.target_id]
        safe_classes = hmd.safe_classes
        for k, it in get_public_exports(hmd.target):
            id_it = id(it)
            cls = it.__class__ if hasattr(it, '__class__') else type(it)
            if id_it in hooks:
#                print 'Hooked: ', k, it.__class__
                setattr(ns, k, hooks[id_it])
            elif id_it in safe_items or cls in safe_classes:
#                print 'Safe: ', k, it.__class__
                setattr(ns, k, it)
            else:
#                print 'Dangerous: ', k, it.__class__
                dangerous_items[id_it] = it
        # print ns

    md = module.__dict__
    for k, v in list(md.iteritems()):
        id_v = id(v)
        if id_v in hooks:
            restore.append((k, md[k]))
            md[k] = hooks[id_v]
            print '{} (imported as {}) hooked'.format(v.__name__, k)
        elif id_v in dangerous_items:
            raise AssertionError('Don\'t know how to hook {} ({!r})!'.format(k, v))
    for k, v in builtin_overrides:
        if k in md: 
            restore.append((k, md[k]))
        else:
            restore.append((k, getattr(__builtin__, k)))
        md[k] = v 
    return restore

