" Vim indent file
" Language:    Pascal
" Maintainer:  Neil Carter <n.carter@swansea.ac.uk>
" Created:     2004 Jul 13
" Last Change: 2010 Jul 14
" by Fj.

let s:debug = 1

if exists("b:did_indent") && !s:debug
    finish
endif
let b:did_indent = 1

setlocal indentexpr=GetPascalIndent(v:lnum)
setlocal indentkeys&
setlocal indentkeys+==end;,==const,==type,==var,==begin,==repeat,==until,==for
setlocal indentkeys+==program,==function,==procedure,==object,==private
setlocal indentkeys+==record,==if,==else,==case

if exists("*GetPascalIndent") && !s:debug
    finish
endif

let s:section_header = '^\s*\(program\|uses\|const\|var\|type\|private\|public\|except\)\>'
let s:function_declaration = '^\s*\(function\|procedure\)\>'
let s:structure_declaration = '\<\(class\|record\)\>\(\s*([^)]*)\s*\)\?$'



function! s:GetPrevNonCommentLineNum( line_num )
    " Skip lines starting with a comment
    let SKIP_LINES = '^\s*\(\((\*\)\|\(\*\ \)\|\(\*)\)\|{\|}\)'

    let nline = a:line_num
    while nline > 0
        let nline = prevnonblank(nline-1)
        if getline(nline) !~? SKIP_LINES
            break
        endif
    endwhile

    return nline
endfunction

function! s:in_class_declaration( line_num )
    " Wouldn't work with nested classes btw.
    let nline = a:line_num
    let counter = 100
    while nline > 0
        let counter -= 1
        if counter < 0
            return 1 " better not to break anything
        endif

        let nline = prevnonblank(nline-1)
        let line = getline(nline)
        if line =~? s:structure_declaration
            return 1
        elseif line =~? '^\s*end\>'
            return 0
        endif
    endwhile
endfunction

function! s:match_any(s, ...)
    for x in a:000
        if a:s =~? x
            return 1
        endif
    endfor
endfunction

function! s:matchcnt(s, pattern)
    let cnt = 0
    let pos = 0
    while 1
        let pos = match(a:s, a:pattern, pos)
        if pos < 0
            return cnt
        endif
        let pos += 1
        let cnt += 1
    endwhile
endfunction

function! GetPascalIndent( line_num )
    " Line 0 always goes at column 0
    if a:line_num == 0
        return 0
    endif

    let this_codeline = getline( a:line_num )

    " If in the middle of a three-part comment
    if this_codeline =~? '^\s*\*'
        return indent( a:line_num )
    endif

    if match_any(this_codeline, '^\s*{\(\$IFDEF\|\$ELSE\|\$ENDIF\)', '^\s*program\>')
        return 0
    endif

    let prev_codeline_num = s:GetPrevNonCommentLineNum( a:line_num )
    let prev_codeline = getline( prev_codeline_num )
    let indnt = indent( prev_codeline_num )
    
    if s:match_any(this_codeline, '^\s*{\(\$IFDEF\|\$ELSE\|\$ENDIF\)', '^\s*\(program\|interface\|implementation\)\>')
        return 0
    endif

    if s:match_any(prev_codeline, s:section_header, s:structure_declaration, '\<\(begin\|repeat\|case\|try\)\s*$')
        let indnt += &sw
    endif

    if s:match_any(this_codeline, s:section_header, '^\s*\(end\|until\)\>', )
        let indnt -= &sw
    elseif this_codeline =~? '^\s*begin\s*$' && prev_codeline !~ '\<\(then\|do\)'
        " the begin that marks function body.
        let indnt -= &sw
    elseif this_codeline =~? s:function_declaration
        if !s:in_class_declaration(prev_codeline_num)
            return 0
        endif " otherwise do nothing.
    endif

    let paren_balance = s:matchcnt(prev_codeline, '[(\[]') - s:matchcnt(prev_codeline, '[)\]]')
    if paren_balance < 0 
        let indnt -= &sw
    elseif paren_balance > 0
        let indnt += &sw
    endif

    return indnt
endfunction

