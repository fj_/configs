" my filetype file
if exists("did_load_filetypes")
    finish
endif

augroup filetypedetect
    au! BufRead,BufNewFile *.ecpp       setf cpp
    au! BufRead,BufNewFile *.zcpp       setf cpp
    au! BufRead,BufNewFile *.ud         setf ud32
    au! BufRead,BufNewFile *.objd       setf objdasm
augroup END

