import sys
from contextlib import contextmanager
from cStringIO import StringIO

class DevNull(object):
    def write(self, s): pass
    def flush(self): pass

@contextmanager
def test(test, stdin, stdout = None, stderr = None):
    stored = sys.stdin, sys.stdout, sys.stderr
    try:
        sys.stdin = input = StringIO(stdin)
        
        if stdout is DevNull: sys.stdout = DevNull()
        elif stdout is not None: sys.stdout = mockstdout = StringIO()
        
        if stderr is DevNull: sys.stderr = DevNull()
        elif stderr is not None: sys.stderr = mockstderr = StringIO()
        
        yield
    finally:
        sys.stdin, sys.stdout, sys.stderr = stored
    test.assertEquals(input.read(), '')
    if stdout is not None and stdout is not DevNull:
        test.assertMultiLineEqual(stdout, mockstdout.getvalue())
    if stderr is not None and stderr is not DevNull:
        test.assertMultiLineEqual(stderr, mockstderr.getvalue())
