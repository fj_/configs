#!/usr/bin/env python

from distutils.core import setup
setup(name='simple_settings',
      version='0.1',
      py_modules=['simple_settings'],
      )
