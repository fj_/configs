#!/bin/bash
set -xeuo pipefail

START=_vim/pack/packages/start

gen_file_whitelist() {
    local lst=$(cat <<EOF
after/
autoload/
compiler/
doc/
extras/
ftdetect/
indent/
plugin/
syntax/
README.*
package\.sh
EOF
)
    # returns patterns above concatenated with '|' to be used as egrep pattern
    local res=
    for line in $lst; do
        if [[ -n $res ]]; then
            res=$res'|'
        fi
        res=$res$line
    done
    echo "$res"
} 2>/dev/null # disables xtrace

FILE_WHITELIST=$(gen_file_whitelist)


update() {
    local url="$1/tarball/master"
    local reponame="$(basename $1)"
    local repodir="$START/$reponame"
    local archive="$START/$reponame.tgz"
    if [[ -e "$repodir" ]]; then
        rm -r "$repodir"
    fi
    mkdir -p "$repodir"
    curl -L "$url" > "$archive"
    tar -tf "$archive" | egrep '^[^/]*/('"$FILE_WHITELIST"')$' \
        | tar -C $repodir -xvzf "$archive" --strip-components=1 --files-from=-
    rm "$archive"
    if [[ -e $repodir/doc ]]; then
        vim -c "helptags $repodir/doc | q"
    fi
    echo
}


#update https://github.com/sjl/gundo.vim
#update https://github.com/will133/vim-dirdiff
#update https://github.com/pangloss/vim-javascript
#update https://github.com/mxw/vim-jsx
#update https://github.com/jlanzarotta/bufexplorer
#update https://github.com/cespare/vim-toml
update https://github.com/scrooloose/nerdcommenter


