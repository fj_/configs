from __future__ import print_function, division
import os
import atexit

try:
    import readline
except:
    print('{0!r}: no readline module'.format(__file__))
else:
    import rlcompleter # automatically installs itself
    history_file = os.path.join(os.path.expanduser("~"), ".python_history")
    try:
        readline.read_history_file(history_file)
    except IOError as exc:
        print('Can\'t load history file {0!r}: {1}'.format(history_file, exc))
        pass
    readline.parse_and_bind("tab: complete")
    atexit.register(readline.write_history_file, history_file)


print('{0!r} loaded'.format(__file__))
