#!/usr/bin/env python3

from winreg_wrapper import RegKey
from os import path as os_path
from itertools import chain
import re

import colorschemes

def allow_Consolas_as_console_font():
    # Only necessary on XP, needs restart.
    with RegKey(r'HKLM\Software\Microsoft\Windows NT\CurrentVersion\Console\TrueTypeFont',
            True) as k:
        n = '0'
        while True:
            s = k.get(n)
            if s is None:
                k[n] = 'Consolas'
                break
            elif s == 'Consolas':
                break
            n += '0'

def set_console_params(colorscheme):
    with RegKey(r'hkcu\Console', write=True) as k:
        for i, c in enumerate(colorschemes.as_int(colorscheme)):
            k['ColorTable%02d' % i] = c
        k['ScreenColors'] = 0xF0
        k['PopupColors'] = 0xF5 # it's the thing that pops up on F7 for example.
        k['FaceName'] = 'Consolas'
        k['FontFamily'] = 54
        k['FontSize'] = 0x100000
        k['FontWeight'] = 400
        width = 100
        k['ScreenBufferSize'] = 0x10000 * 2000 + width
        k['WindowSize'] = 0x10000 * 40 + width

def set_putty_params(colorscheme, target_session=None, exception_session=None):
    cs = colorscheme
    ix = colorschemes.index
    cs = [
            cs[ix.Black],      # default foreground
            cs[ix.Grey],       # default bold foreground.
            cs[ix.White],      # default background
            cs[ix.LightGrey],  # default bold background.
            cs[ix.White],      # cursor text
            cs[ix.Black],      # cursor color
#            '#FFFFFF',         # cursor text
#            '#000000',         # cursor color
            cs[ix.Black],
            cs[ix.Grey],
            cs[ix.Red],
            cs[ix.LightRed],
            cs[ix.Green],
            cs[ix.LightGreen],
            cs[ix.Brown],
            cs[ix.Yellow],
            cs[ix.Blue],
            cs[ix.LightBlue],
            cs[ix.Magenta],
            cs[ix.LightMagenta],
            cs[ix.Cyan],
            cs[ix.LightCyan],
            cs[ix.LightGrey],
            cs[ix.White]]
    cs = colorschemes.as_tuple(cs)
    d = [('Colour%d' % i, '%d,%d,%d' % c) for i, c in enumerate(cs)]

    with RegKey(r'HKCU\Software\SimonTatham\PuTTY\Sessions') as sessions:
        for session_name in sessions.subkeys():
            if target_session is not None and session_name != target_session: continue
            if session_name == exception_session: continue
            with sessions.open(session_name, True) as session:
                print('******', session['HostName'])
                session.update(d)
                # remove the annoying attempt to login with public key
                session['AuthGSSAPI'] = 0
                session['AuthKI'] = 0
                session['GssapiFwd'] = 0
                session['X11AuthFile'] = ''

                session['TermHeight'] = 40
                session['TermWidth'] = 88
                session['BlinkCur'] = 1
                session['BoldAsColour'] = 1
                session['TermHeight'] = 40
                session['BugMaxPkt2'] = 0
                # kinda sucks because then I can't close the first putty, but whatever.
                session['ConnectionSharing'] = 1
                session['ConnectionSharingDownstream'] = 1
                session['ConnectionSharingUpstream'] = 1
                session['CRImpliesLF'] = 0
                session['Font'] = 'Consolas'
                session['FontHeight'] = 10
                session['FontVTMode'] = 1
                session['KEX'] = 'dh-gex-sha1,dh-group14-sha1,dh-group1-sha1,WARN,rsa'
                session['LineCodePage'] = 'UTF-8'
                session['LogFileClash'] = 0xFFFFFFFF
                session['LogFileName'] = r'C:\temp\putty.log'
                session['X11Forward'] = 1
                session['Compression'] = 1
                session['LogHost'] = ''
                session['UserNameFromEnvironment'] = 0
                session['ScrollbackLines'] = 1000
                session['TerminalType'] = 'xterm'
                session['NoApplicationKeys'] = 1 # easier than trying to teach vim to understand numpad
# On a second thought, I like my tango-based colours better.
#                session['TerminalType'] = 'xterm-256color'


def list_putty_differences(template_session):
    with RegKey(r'HKCU\Software\SimonTatham\PuTTY\Sessions') as sessions:
        with sessions.open(template_session) as k:
            template_values = dict((n, v) for n, v, _ in k)
        tv_sorted = sorted(template_values.items())

        for session_name in sessions.subkeys():
            with sessions.open(session_name) as session:
                print('******', session_name, ':', session['HostName'])
                cur_values = dict((n, v) for n, v, _ in session)
                extra_keys = cur_values.keys() - template_values.keys()
                if extra_keys:
                    print('Extra keys:')
                    for key in sorted(extra_keys):
                        print('{} {!r}'.format(key, session[key]))
                diff = [(k, v, v1)
                        for k, v in tv_sorted
                        if k not in ('HostName', 'UserName', 'WinTitle', 'TerminalModes')
                        for v1 in [cur_values.get(k)]
                        if v1 != v]
                if diff:
                    print('Different values:')
                    for x in diff:
                        print('{} {!r} {!r}'.format(*x))

def patch_console2_settings(colorscheme):
    replacement = '\n'.join(
            ['<colors>'] +
            ['\t\t\t<color id="%d" r="%d" g="%d" b="%d"/>' % (n, r, g, b)
                    for n, (r, g, b) in enumerate(colorschemes.as_tuple(colorscheme))] +
            ['\t\t</colors>'])


    fname = r'C:\Program Files\Console2\bin\console.xml'
    if not os_path.exists(fname):
        fname = r'C:\Program Files (x86)\Console2\bin\console.xml'
    with open(fname, 'r') as f:
        data = f.read()
    data, n = re.subn(r'<colors>(.|\n)*</colors>', replacement, data)
    print(data)
    assert n == 1
    with open(fname, 'w') as f:
        f.write(data)


def main():
    colorscheme = colorschemes.tango_contrast
#    colorscheme = colorschemes.windows_default
#    allow_Consolas_as_console_font()
    set_console_params(colorscheme)
    set_putty_params(colorscheme)
#    patch_console2_settings(colorscheme)

    print('\nVerifying\n')
    list_putty_differences('jackdaniels.konts.lv')

if __name__ == '__main__':
    main()
    print('Yay!')
