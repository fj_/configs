#!/usr/bin/env python

# useful for testing scripts

scary_name = ' ~` !@#$%^&*()_+[]{};:\'"\\|,<.>?\n\\ \\\\ \\\\\\ \\'
# important touches are: space in the beginning and inside the string,
# \n, multiple backslashes, terminating backslash (bugs Vim's 
# `:e \\ | echo "zzz"`)
scary_name2 = scary_name.replace('\n', '') # hg doesn't like it.

open(scary_name, 'w').write('1\n2\n')
open(scary_name2, 'w').write('1\n2\n')
