import os
import os.path as os_path
from cStringIO import StringIO
from copy import deepcopy
from contextlib import contextmanager
import module_hooks
from module_hooks import HookedModuleDefinition
import glob
import fnmatch
import tempfile

#def compare_FS_dicts(d1, d2, path=''):
#    def add_path(msg):
#        fixed_path = path if path else '/'
#        return '{}: {}'.format(fixed_path, msg)
#    s1, s2 = set(d1), set(d2)
#    if s1 != s2:
#        return add_path('Difference in keys, extra: {}, absent: {}'.format(s2 - s1, s1 - s2))
#    for k, v1 in d1.iteritems():
#        v2 = d2[k]
#        is_string = isinstance(v1, basestring), isinstance(v2, basestring)
#        if is_string == (True, True):
#            if v1 != v2:
#                return add_path('File "{}" has different contents, original:\n{}\nnew:\n{}'
#                                .format(k, v1, v2))
#        elif is_string == (True, False):
#            return add_path('"{}" was file, is directory now'.format(k))
#        elif is_string == (False, True):
#            return add_path('"{}" was directory, is file now'.format(k))
#        else:
#            res = compare_FS_dicts(v1, v2, path + '/' + k)
#            if res: return res

#def update_FS_dicts(base, update):
#    for k, v in update.iteritems():
#        if v is None:
#            del base[k]
#        elif k not in base or isinstance(base[k], basestring) or isinstance(v, basestring):
#            base[k] = deepcopy(v)
#        else:
#            update_FS_dicts(base[k], v)

def iter_FS_dict(d, path = ''):
    for k, v in d.iteritems():
        if isinstance(v, basestring):
            yield path + k
        else:
            for res in iter_FS_dict(v, path + k + '/'):
                yield res

def pformat_FS_dict(d, indent='', res = None):
    r'''Pretty-format mock filesystem contents.
    
    >>> pformat_FS_dict({'a':'b', 'c': {}, 'd': {'e':''}, 'b':'1\n', 'c':'', 'e':'\n'})
    'd/ (1 items)\n    e (0 bytes)\n        \na (1 bytes)\n    b\nb (2 bytes)\n    1\n    \nc (0 bytes)\n    \ne (1 bytes)\n    \n    \n'
    '''
    toplevel = False
    if res is None:
        res = StringIO()
        toplevel = True
    
    dirs, files = [], []
    for k, v in d.iteritems():
        if isinstance(v, basestring):
            files.append((k, v))
        else:
            dirs.append((k, v))
    dirs.sort()
    files.sort()
    next_indent = indent + '    '
    for k, v in dirs:
        res.write('{}{}/ ({} items)\n'.format(indent, k, len(v)))
        pformat_FS_dict(v, next_indent, res)
    for k, v in files:
        res.write('{}{} ({} bytes)\n'.format(indent, k, len(v)))
        for line in v.split('\n'):
            res.write('{}{}\n'.format(next_indent, line))
            
    if toplevel: return res.getvalue()

class MockFileHandle(object):
    '''Care should be taken to not leak or store handles, because
    they don't lock parent directories or anything'''
    def __init__(self, parent_dir, name, path):
        self.parent_dir = parent_dir
        self.name = name
        self.path = path
    def get(self):
        return self.parent_dir[self.name]
    def set(self, value):
        self.parent_dir[self.name] = value
    def exists(self):
        return self.name in self.parent_dir
    def isfile(self):
        return self.exists() and isinstance(self.get(), basestring)
    def isdir(self):
        return self.exists() and not isinstance(self.get(), basestring)
    def delete(self):
        del self.parent_dir[self.name]

class MockFile(object):
    def __init__(self, parent_fs, handle, mode):
        self.parent_fs = parent_fs
        self.handle = handle
        self.closed = False
        self.mode = mode
        self.readonly = self.mode == 'r'
        if self.readonly:
            assert handle.exists(), 'File doesn\'t exist: {}'.format(handle.path)
            assert handle.isfile(), 'Can\'t open a directory: {}'.format(handle.path)
            self.buffer = StringIO(handle.get())
        else:
            assert not handle.exists() or handle.isfile(), 'Can\'t open a directory: {}'.format(handle.path)
            self.buffer = StringIO()
            handle.set('')
        self.parent_fs._lock(handle)
        # AttributeError: 'cStringIO.StringI' object has no attribute 'write'
        buffer = self.buffer
        if self.readonly:
            self._methods = { 'read' : buffer.read, 'next' : buffer.next,
                    'readline' : buffer.readline, 'readlines' : buffer.readlines}
        else:
            self._methods = { 'write' : buffer.write, 'flush' : buffer.flush,
                    'writelines' : buffer.writelines}
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
    def close(self):
        if self.closed: return
        if not self.readonly:
            self.handle.set(self.buffer.getvalue())
        self.buffer.close()
        self.parent_fs._unlock(self.handle)
    def __getattr__(self, name):
        # delegate some methods to the underlying buffer
        assert not self.closed, '{}: file is closed: {}'.format(name, self.handle.path)
        m = self._methods.get(name, None)
        if m: return m
        raise AttributeError('{}: attribute not found on file opened for {}: {}'
                .format(name, 'reading' if self.readonly else 'writing', self.handle.path))

class MockFD(object):
    def __init__(self, mock_file):
        self.mock_file = mock_file

class MockFS(object):
    """A simple mock file system, intended to be used for comparing
    actual and intended results rather than sequences of actions.
    
    FS representation is a possibly nested dictionary of strings, 
    no soft/hard links are supported.
    
    For obvious reasons, the class is rather picky about what it'd 
    accept as valid paths.
    """
    ################
    # administrative functions
    def reset(self):
        self.contents = deepcopy(self.original_contents)
        self.locked_files = set()

    def __init__(self, contents):
        self.temp_counter = 0
        self.original_contents = contents
        self.reset()
        if os.name == 'nt':
            self.initial_directory = r'Q:\Program Files\Test'
        else:
            self.initial_directory = '/mount/Device Name/test'
        self.hook_dictionary = None

    def install(self, module):
        '''Replaces all interesting things in can find on module level with hooks,
        or throws an exception if it doesn't have a suitable hook. See init_hooks and
        especially safe_items variable there to mark items as not interesting. 

        Returns a list [(key, original_value), ...] which can be used to restore
        original values, simply as `module.__dict__.update(original)`. Though why 
        would anyone want to do that...
        '''
        hooks = (
                HookedModuleDefinition(os_path, [
                        (os_path.isfile, self.isfile),
                        (os_path.abspath, self.abspath),
                        (os_path.exists, self.exists),
                        ], [os_path.dirname, os_path.basename]),
                                
                HookedModuleDefinition(os, [
                        (os.fdopen, self.fdopen),
                        (os.remove, self.remove),
                        (os.rename, self.rename),
                        ],),
                
                HookedModuleDefinition(glob, [
                        (glob.glob, self.glob),
                        ]),
                        
                HookedModuleDefinition(tempfile, [
                        (tempfile.mkstemp, self.mkstemp)]),
                )
        return module_hooks.install(module, [
                ('open', self.open),
                ], *hooks)
        
    def apply_changes(self, target_dict, changes):
        if isinstance(changes, dict): changes = changes.iteritems()
        for k, v in changes:
            if v is None:
                # delete
                h = self._find(k)
                if h and h.exists():
                    h.delete()
                continue
            # add/update
            lst, _ = self._parse_path(k)
            assert len(lst)
            d = target_dict
            for it in lst[:-1]:
                if it in d: 
                    d = d[it]
                else: 
                    d[it] = newd = {}
                    d = newd
            d[lst[-1]] = deepcopy(v)
                    
            

    @contextmanager
    def test(self, testcase, expected_difference):
        self.reset()
        yield
        testcase.assertFalse(self.locked_files,
                'Some files weren\'t closed: {}'.format(self.locked_files))
        expected = deepcopy(self.original_contents)
        self.apply_changes(expected, expected_difference)
        testcase.assertMultiLineEqual(pformat_FS_dict(expected), pformat_FS_dict(self.contents))


    ################
    # support functions

    def _parse_path(self, path):
        path = self.abspath(path)
        assert path.startswith(self.initial_directory), (
                'Supplied path is not relative to the base directory ({}): {}'
                .format(self.initial_directory, path))
        path = path[len(self.initial_directory) + 1:] # cut the separator as well
        components = []
        while path:
            path, it = os_path.split(path)
            components.append(it)
        components.reverse()
        return components, os_path.join(*components)

    def _find(self, path):
        lst, path = self._parse_path(path)
        assert len(lst), 'can\'t reference the root directory!'
        d = self.contents
        for it in lst[:-1]:
            if it in d: d = d[it]
            else: return None
        return MockFileHandle(d, lst[-1], path)

    def _lock(self, handle):
        path = handle.path
        assert path not in self.locked_files, 'File already open: {}'.format(path)
        self.locked_files.add(path)

    def _unlock(self, handle):
        path = handle.path
        assert path in self.locked_files, 'File not open: {}'.format(path)
        self.locked_files.remove(path)

    ################
    # mock functions
    
    def rename(self, source, target):
        'Follows Windows (broken) behaviour -- the target must not exist' 
        source = self._find(source)
        assert source and source.exists()
        assert source.isfile()
        assert source.path not in self.locked_files
        target = self._find(target)
        assert target
        assert not target.exists()
        target.set(source.get())
        source.delete()
        
    def remove(self, path):
        handle = self._find(path)
        assert handle and handle.exists()
        assert handle.isfile()
        assert handle.path not in self.locked_files
        handle.delete()
    
    def fdopen(self, fd, mode = None):
        assert isinstance(fd, MockFD)
        f = fd.mock_file
        assert f.mode is None or f.mode == mode
        return f
        
    def mkstemp(self, dir, text=True):
        self.temp_counter += 1
        path = os_path.join(dir, '!!!!tempfile.{}'.format(self.temp_counter))
        handle = self._find(path)
        assert handle, 'Path doesn\'t exist: {}'.format(path)
        assert not handle.exists()
        file = MockFile(self, handle, 'w')
        fd = MockFD(file)
        return fd, self.abspath(file.handle.path)

    def abspath(self, path):
        path = os_path.join(self.initial_directory, path)
        return os_path.normpath(path)

    def exists(self, path):
        f = self._find(path)
        return f and f.exists()

    def isfile(self, path):
        f = self._find(path)
        return f and f.isfile()

    def isdir(self, path):
        f = self._find(path)
        return f and f.isdir()
    
    def open(self, path, mode):
        assert mode in ('r', 'w')
        handle = self._find(path)
        assert handle, 'Path doesn\'t exist: {}'.format(path)
        return MockFile(self, handle, mode)

    def glob(self, pattern):
        components, _ = self._parse_path(pattern)
        def recurse(d, remainder, path):
            if not len(remainder):
                return [path]
            component, remainder = remainder[0], remainder[1:]
            if not component:
                # matching a directory
                assert not len(remainder)
                return path + '/'
            if isinstance(d, basestring):
                return [] 
            res = []
            for k, v in d.iteritems():
                if fnmatch.fnmatch(k, component):
                    res.extend(recurse(v, remainder, os_path.join(path, k)))
            return res
        return recurse(self.contents, components, '')
                
    iglob = glob

if __name__ == '__main__':
    import doctest
    doctest.testmod()
