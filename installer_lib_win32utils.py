from ctypes import windll, Structure, c_byte, WinError
from ctypes.wintypes import BOOL, DWORD, USHORT, UINT, LPCSTR, FILETIME, HANDLE, LPVOID, POINTER

#BOOL WINAPI CreateHardLink(
#  __in        LPCTSTR lpFileName,
#  __in        LPCTSTR lpExistingFileName,
#  __reserved  LPSECURITY_ATTRIBUTES lpSecurityAttributes
#);

CreateHardLink = windll.kernel32.CreateHardLinkA
CreateHardLink.argtypes = (LPCSTR, LPCSTR, LPVOID)
CreateHardLink.restype = BOOL

#HANDLE WINAPI CreateFile(
#  __in      LPCTSTR lpFileName,
#  __in      DWORD dwDesiredAccess,
#  __in      DWORD dwShareMode,
#  __in_opt  LPSECURITY_ATTRIBUTES lpSecurityAttributes,
#  __in      DWORD dwCreationDisposition,
#  __in      DWORD dwFlagsAndAttributes,
#  __in_opt  HANDLE hTemplateFile
#);
CreateFile = windll.kernel32.CreateFileA
CreateFile.argtypes = (LPCSTR, DWORD, DWORD, LPVOID, DWORD, DWORD, LPVOID)
CreateFile.restype = HANDLE

#BOOL WINAPI CloseHandle(
#  __in  HANDLE hObject
#);
CloseHandle = windll.kernel32.CloseHandle
CloseHandle.argtypes = (HANDLE,)
CloseHandle.restype = BOOL

class FileHandle(object):
    def __init__(self, fileName, desiredAccess, shareMode, creationDispositon, flagsAndAttributes):
        self.handle = CreateFile(fileName, desiredAccess, shareMode, 
                                0, creationDispositon, flagsAndAttributes, 0)
        if not self.handle:
            raise WinError()
    @property
    def _as_parameter_(self): 
        return self.handle
    def close(self):
        if self.handle:
            CloseHandle(self.handle)
            self.handle = None
    def __del__(self):
        self.close()
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

#typedef struct _BY_HANDLE_FILE_INFORMATION {
#  DWORD    dwFileAttributes;
#  FILETIME ftCreationTime;
#  FILETIME ftLastAccessTime;
#  FILETIME ftLastWriteTime;
#  DWORD    dwVolumeSerialNumber;
#  DWORD    nFileSizeHigh;
#  DWORD    nFileSizeLow;
#  DWORD    nNumberOfLinks;
#  DWORD    nFileIndexHigh;
#  DWORD    nFileIndexLow;
#}BY_HANDLE_FILE_INFORMATION, *PBY_HANDLE_FILE_INFORMATION;
class By_handle_file_information(Structure):
    _fields_ = (
        ('dwFileAttributes', DWORD), 
        ('ftCreationTime', FILETIME),
        ('ftLastAccessTime', FILETIME),
        ('ftLastWriteTime', FILETIME),
        ('dwVolumeSerialNumber', DWORD),
        ('nFileSizeHigh', DWORD),
        ('nFileSizeLow', DWORD),
        ('nNumberOfLinks', DWORD),
        ('nFileIndexHigh', DWORD),
        ('nFileIndexLow', DWORD))
    
#BOOL WINAPI GetFileInformationByHandle(
#  __in   HANDLE hFile,
#  __out  LPBY_HANDLE_FILE_INFORMATION lpFileInformation
#);
GetFileInformationByHandle = windll.kernel32.GetFileInformationByHandle
GetFileInformationByHandle.argtypes = (HANDLE, POINTER(By_handle_file_information))
GetFileInformationByHandle.restype = BOOL

# The following stuff is about junction creation and was translated from
# http://www.codeproject.com/KB/files/JunctionPointsNet.aspx


ERROR_NOT_A_REPARSE_POINT = 4390
ERROR_REPARSE_ATTRIBUTE_CONFLICT = 4391
ERROR_INVALID_REPARSE_DATA = 4392
ERROR_REPARSE_TAG_INVALID = 4393
ERROR_REPARSE_TAG_MISMATCH = 4394
FSCTL_SET_REPARSE_POINT = 0x000900A4
FSCTL_GET_REPARSE_POINT = 0x000900A8
FSCTL_DELETE_REPARSE_POINT = 0x000900AC
IO_REPARSE_TAG_MOUNT_POINT = 0xA0000003
NonInterpretedPathPrefix = '\\??\\'
    
class EFileAccess:
    GenericRead = 0x80000000
    GenericWrite = 0x40000000
    GenericExecute = 0x20000000
    GenericAll = 0x10000000

class EFileShare:
    None_ = 0x00000000
    Read = 0x00000001
    Write = 0x00000002
    Delete = 0x00000004

class ECreationDisposition:
    New = 1
    CreateAlways = 2
    OpenExisting = 3
    OpenAlways = 4
    TruncateExisting = 5

class EFileAttributes:
    Readonly = 0x00000001
    Hidden = 0x00000002
    System = 0x00000004
    Directory = 0x00000010
    Archive = 0x00000020
    Device = 0x00000040
    Normal = 0x00000080
    Temporary = 0x00000100
    SparseFile = 0x00000200
    ReparsePoint = 0x00000400
    Compressed = 0x00000800
    Offline = 0x00001000
    NotContentIndexed = 0x00002000
    Encrypted = 0x00004000
    Write_Through = 0x80000000
    Overlapped = 0x40000000
    NoBuffering = 0x20000000
    RandomAccess = 0x10000000
    SequentialScan = 0x08000000
    DeleteOnClose = 0x04000000
    BackupSemantics = 0x02000000
    PosixSemantics = 0x01000000
    OpenReparsePoint = 0x00200000
    OpenNoRecall = 0x00100000
    FirstPipeInstance = 0x00080000

class REPARSE_DATA_BUFFER(Structure):
    _fields_ = (
        # Reparse point tag. Must be a Microsoft reparse point tag.
        ('ReparseTag', UINT),
    
        # Size, in bytes, of the data after the Reserved member. This can be calculated by:
        # (4 * sizeof(ushort)) + SubstituteNameLength + PrintNameLength + 
        # (namesAreNullTerminated ? 2 * sizeof(char) : 0);
        ('ReparseDataLength', USHORT),
    
        # Reserved; do not use. 
        ('Reserved', USHORT),
    
        # Offset, in bytes, of the substitute name string in the PathBuffer array.
        ('SubstituteNameOffset', USHORT),
    
        # Length, in bytes, of the substitute name string. If this string is null-terminated,
        # SubstituteNameLength does not include space for the null character.
        ('SubstituteNameLength', USHORT),
    
        # Offset, in bytes, of the print name string in the PathBuffer array.
        ('PrintNameOffset', USHORT),
    
        # Length, in bytes, of the print name string. If this string is null-terminated,
        # PrintNameLength does not include space for the null character. 
        ('PrintNameLength', USHORT),
    
        # A buffer containing the unicode-encoded path string. The path string contains
        # the substitute name string and print name string.
        # length is arbitrary, makes the whole struct to be 0x4000 (header is 16 bytes)
        ('PathBuffer', c_byte * 0x3FF0))
    
#BOOL WINAPI DeviceIoControl(
#  __in         HANDLE hDevice,
#  __in         DWORD dwIoControlCode,
#  __in_opt     LPVOID lpInBuffer,
#  __in         DWORD nInBufferSize,
#  __out_opt    LPVOID lpOutBuffer,
#  __in         DWORD nOutBufferSize,
#  __out_opt    LPDWORD lpBytesReturned,
#  __inout_opt  LPOVERLAPPED lpOverlapped
#);    
DeviceIoControl = windll.kernel32.DeviceIoControl
DeviceIoControl.argtypes = (HANDLE, DWORD, LPVOID, DWORD, LPVOID, DWORD, POINTER(DWORD), LPVOID) 
DeviceIoControl.restype = BOOL

def open_reparse_point(reparsePoint, accessMode):
    return FileHandle(reparsePoint, accessMode,
                EFileShare.Read | EFileShare.Write | EFileShare.Delete,
                ECreationDisposition.OpenExisting,
                EFileAttributes.BackupSemantics | EFileAttributes.OpenReparsePoint)
        
def throwLastWin32Error(message = None):
    exc = WinWerror() # uses lasterror
    if message:
        exc.descr = message + ' ' + exc.descr
    raise message


# Creates a junction point from the specified directory to the specified target directory.
# <remarks>
# Only works on NTFS.
# </remarks>
# <param name="junctionPoint">The junction point path</param>
# <param name="targetDir">The target directory</param>
# <param name="overwrite">If true overwrites an existing reparse point or empty directory</param>
# <exception cref="IOException">Thrown when the junction point could not be created or when
# an existing directory was found and <paramref name="overwrite" /> if false</exception>
#def junction_create(junctionPoint, targetDir, overwrite = True):
#    targetDir = os_path.abspath(targetDir)
#    if not os_path.exists(targetDir):
#        raise IOError('Target path does not exist or is not a directory.')
#
#    if os_path.exists(junctionPoint):
#        if not overwrite:
#            raise IOError('Directory already exists and overwrite parameter is false.')
#        os.mkdir(junctionPoint)
#        
#    wi (SafeFileHandle handle = OpenReparsePoint(junctionPoint, EFileAccess.GenericWrite))
#                byte[] targetDirBytes = Encoding.Unicode.GetBytes(NonInterpretedPathPrefix + Path.GetFullPath(targetDir));
#
#        REPARSE_DATA_BUFFER reparseDataBuffer = new REPARSE_DATA_BUFFER();
#
#        reparseDataBuffer.ReparseTag = IO_REPARSE_TAG_MOUNT_POINT;
#        reparseDataBuffer.ReparseDataLength = (ushort)(targetDirBytes.Length + 12);
#        reparseDataBuffer.SubstituteNameOffset = 0;
#        reparseDataBuffer.SubstituteNameLength = (ushort)targetDirBytes.Length;
#        reparseDataBuffer.PrintNameOffset = (ushort)(targetDirBytes.Length + 2);
#        reparseDataBuffer.PrintNameLength = 0;
#        reparseDataBuffer.PathBuffer = new byte[0x3ff0];
#        Array.Copy(targetDirBytes, reparseDataBuffer.PathBuffer, targetDirBytes.Length);
#
#        int inBufferSize = Marshal.SizeOf(reparseDataBuffer);
#        IntPtr inBuffer = Marshal.AllocHGlobal(inBufferSize);
#
#        try
#                        Marshal.StructureToPtr(reparseDataBuffer, inBuffer, false);
#
#            int bytesReturned;
#            bool result = DeviceIoControl(handle.DangerousGetHandle(), FSCTL_SET_REPARSE_POINT,
#                inBuffer, targetDirBytes.Length + 20, IntPtr.Zero, 0, out bytesReturned, IntPtr.Zero);
#
#            if (!result)
#                ThrowLastWin32Error("Unable to create junction point.");
#                    finally
#                        Marshal.FreeHGlobal(inBuffer);
                        
## Deletes a junction point at the specified source directory along with the directory itself.
## Does nothing if the junction point does not exist.
## <remarks>
## Only works on NTFS.
## </remarks>
## <param name="junctionPoint">The junction point path</param>
#public static void Delete(string junctionPoint)
#        if (!Directory.Exists(junctionPoint))
#                if (File.Exists(junctionPoint))
#            throw new IOException("Path is not a junction point.");
#
#        return;
#        
#    using (SafeFileHandle handle = OpenReparsePoint(junctionPoint, EFileAccess.GenericWrite))
#                REPARSE_DATA_BUFFER reparseDataBuffer = new REPARSE_DATA_BUFFER();
#
#        reparseDataBuffer.ReparseTag = IO_REPARSE_TAG_MOUNT_POINT;
#        reparseDataBuffer.ReparseDataLength = 0;
#        reparseDataBuffer.PathBuffer = new byte[0x3ff0];
#
#        int inBufferSize = Marshal.SizeOf(reparseDataBuffer);
#        IntPtr inBuffer = Marshal.AllocHGlobal(inBufferSize);
#        try
#                        Marshal.StructureToPtr(reparseDataBuffer, inBuffer, false);
#
#            int bytesReturned;
#            bool result = DeviceIoControl(handle.DangerousGetHandle(), FSCTL_DELETE_REPARSE_POINT,
#                inBuffer, 8, IntPtr.Zero, 0, out bytesReturned, IntPtr.Zero);
#
#            if (!result)
#                ThrowLastWin32Error("Unable to delete junction point.");
#                    finally
#                        Marshal.FreeHGlobal(inBuffer);
#            
#        try
#                        Directory.Delete(junctionPoint);
#                    catch (IOException ex)
#                        throw new IOException("Unable to delete junction point.", ex);
#                        
## Determines whether the specified path exists and refers to a junction point.
## <param name="path">The junction point path</param>
## <returns>True if the specified path represents a junction point</returns>
## <exception cref="IOException">Thrown if the specified path is invalid
## or some other error occurs</exception>
#public static bool Exists(string path)
#        if (! Directory.Exists(path))
#        return false;
#
#    using (SafeFileHandle handle = OpenReparsePoint(path, EFileAccess.GenericRead))
#                string target = InternalGetTarget(handle);
#        return target != null;
#            
## Gets the target of the specified junction point.
## <remarks>
## Only works on NTFS.
## </remarks>
## <param name="junctionPoint">The junction point path</param>
## <returns>The target of the junction point</returns>
## <exception cref="IOException">Thrown when the specified path does not
## exist, is invalid, is not a junction point, or some other error occurs</exception>
#public static string GetTarget(string junctionPoint)
#        using (SafeFileHandle handle = OpenReparsePoint(junctionPoint, EFileAccess.GenericRead))
#                string target = InternalGetTarget(handle);
#        if (target == null)
#            throw new IOException("Path is not a junction point.");
#
#        return target;
#            
#private static string InternalGetTarget(SafeFileHandle handle)
#        int outBufferSize = Marshal.SizeOf(typeof(REPARSE_DATA_BUFFER));
#    IntPtr outBuffer = Marshal.AllocHGlobal(outBufferSize);
#
#    try
#                int bytesReturned;
#        bool result = DeviceIoControl(handle.DangerousGetHandle(), FSCTL_GET_REPARSE_POINT,
#            IntPtr.Zero, 0, outBuffer, outBufferSize, out bytesReturned, IntPtr.Zero);
#
#        if (!result)
#                        int error = Marshal.GetLastWin32Error();
#            if (error == ERROR_NOT_A_REPARSE_POINT)
#                return null;
#
#            ThrowLastWin32Error("Unable to get information about junction point.");
#            
#        REPARSE_DATA_BUFFER reparseDataBuffer = (REPARSE_DATA_BUFFER)
#            Marshal.PtrToStructure(outBuffer, typeof(REPARSE_DATA_BUFFER));
#
#        if (reparseDataBuffer.ReparseTag != IO_REPARSE_TAG_MOUNT_POINT)
#            return null;
#
#        string targetDir = Encoding.Unicode.GetString(reparseDataBuffer.PathBuffer,
#            reparseDataBuffer.SubstituteNameOffset, reparseDataBuffer.SubstituteNameLength);
#
#        if (targetDir.StartsWith(NonInterpretedPathPrefix))
#            targetDir = targetDir.Substring(NonInterpretedPathPrefix.Length);
#
#        return targetDir;
#            finally
#                Marshal.FreeHGlobal(outBuffer);
#            


# high-level utilities
def create_hardlink(src, dst):
    assert CreateHardLink(src, dst, 0)
    
def get_file_id(src):
    with FileHandle(src, 0, EFileShare.Read, ECreationDisposition.OpenExisting, 0) as f:
        bhfi = By_handle_file_information()
        res = GetFileInformationByHandle(f, bhfi)
        id = bhfi.dwVolumeSerialNumber
        id = (id << 32) + bhfi.nFileIndexHigh 
        id = (id << 32) + bhfi.nFileIndexLow
        return id 

