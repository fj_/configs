#!/bin/sh

cd vim
./configure \
    --with-features=big \
    --enable-pythoninterp \
    --enable-terminal \
    --enable-gui=gtk2

make clean
make
# make install
