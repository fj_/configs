#!/bin/sh
# vim: set ft=make :
make THIS_MAKEFILE=$0 -f - <<"EOF"

# Note: before compiling do "yum install glibc-devel.i686 libstdc++-devel.i686" required for
# building x32 version of gcc.

#GCC := gcc-4.2.4
#GCC := gcc-4.4.4
#GCC := gcc-4.6.3
#GCC := gcc-4.8.5
#GCC := gcc-6.1.0
#GCC := gcc-7.1.0
#GCC := gcc-5.3.0
GCC := gcc-9.1.0

PREREQ_LIBS := gmp mpfr mpc isl
BUILD_DIR := $(GCC)_build
PREFIX := /usr/local/$(shell echo $(GCC) | tr -c -d [:alnum:])

all: $(BUILD_DIR)/Makefile
	$(MAKE) -C $(BUILD_DIR)


gcc-%: gcc-%.tar.xz
	dtrx $<

gcc-%: gcc-%.tar.gz
	dtrx $<

gcc-%: gcc-%.tar.bz2
	dtrx $<

$(addprefix $(GCC)/%/,$(PREREQ_LIBS)): | $(GCC) # dirty hack: "%" makes it a pattern rule
	cd $(GCC) && withproxy ./contrib/download_prerequisites

$(BUILD_DIR):
	mkdir -p $@

$(BUILD_DIR)/Makefile: $(GCC) $(addprefix $(GCC)/./,$(PREREQ_LIBS)) $(THIS_MAKEFILE) | $(BUILD_DIR)
	cd $(BUILD_DIR) && $(realpath $(GCC))/configure --prefix=$(PREFIX)

EOF
