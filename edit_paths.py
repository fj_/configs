#/bin/env python3

import winreg
from itertools import chain
import traceback
import os
import os.path as os_path
import sys, subprocess
import operator
import ctypes


def doctrim(docstring):
    '''Taken straight from PEP-257'''
    if not docstring:
        return ''
    lines = docstring.expandtabs().splitlines()
    # Determine minimum indentation (first line doesn't count):
    indent = sys.maxsize
    for line in lines[1:]:
        stripped = line.lstrip()
        if stripped:
            indent = min(indent, len(line) - len(stripped))
    # Remove indentation (first line is special):
    trimmed = [lines[0].strip()]
    if indent < sys.maxsize:
        for line in lines[1:]:
            trimmed.append(line[indent:].rstrip())
    # Strip off trailing and leading blank lines:
    while trimmed and not trimmed[-1]:
        trimmed.pop()
    while trimmed and not trimmed[0]:
        trimmed.pop(0)
    # Return a single string:
    return '\n'.join(trimmed)


def parse_cmd(str):
    '''Breaks string into space-separated arguments. Arguments that contain spaces
    can be quoted ('"'), quotes are escaped by doubling.
    Like in windows commandline, basically.

    >>> parse_cmd('"some command" arg1 arg2 "arg3 ""arg 4""\" "" ab""c ab""\""c')
    ['some command', 'arg1', 'arg2', 'arg3 "arg 4"', '', 'abc', 'ab"c']
    '''
    class Args(object):
        def __init__(self):
            self.arg = None
            self.args = []
        def append(self, c):
            if self.arg is None: self.arg = []
            if c: self.arg.append(c) # hack for quoted empty strings
        def shift(self):
            if self.arg is not None:
                self.args.append(''.join(self.arg))
            self.arg = None
    args = Args()
    inquote = False
    wasinquote = False

    for c in str:
        if inquote:
            if c == '"':
                inquote, wasinquote = False, True
            else:
                args.append(c)
        else:
            if c.isspace():
                args.shift()
            elif c == '"':
                inquote = True # that's how it works, I checked
                args.append('"' if wasinquote else None)
            else:
                args.append(c)
            wasinquote = False
    args.shift()
    return args.args


def notify_setting_changed(setting):
    HWND_BROADCAST = -1
    WM_SETTINGCHANGE = 0x1A
    return ctypes.windll.user32.SendMessageW(HWND_BROADCAST, WM_SETTINGCHANGE, 0, setting)


#decorator for adding commands to list
def command(commands):
    def wrapper(f):
        name = f.__name__
        assert name[:4] == 'cmd_'
        commands[name[4:]] = f
        return f
    return wrapper


class ExecutionScope(object):
    commands = {}
    interactive_commands = {}


    def __init__(self):
        self.interactive = True
        self.dirty = False
        self.paths = []
        self.reghive = winreg.HKEY_LOCAL_MACHINE
        self.regkey =  r'SYSTEM\CurrentControlSet\Control\Session Manager\Environment'
        self.env_variable = None
        self.env_type = None


    def load_env_value(self):
        with winreg.OpenKey(self.reghive, self.regkey, 0, winreg.KEY_QUERY_VALUE) as key:
            try:
                value, self.env_type = winreg.QueryValueEx(key, self.env_variable)
            except WindowsError as exc:
                if exc.errno == 2:
                    print(f'Variable {self.env_variable}, doesn\'t exist, assuming REG_SZ type')
                value, self.env_type = '', winreg.REG_SZ
            return value


    def store_env_value(self, value):
        with winreg.OpenKey(self.reghive, self.regkey, 0, winreg.KEY_SET_VALUE) as key:
            winreg.SetValueEx(key, self.env_variable, 0, self.env_type, value)
            print(f'Writing {self.env_variable} = {value!r}')
        # Notify everyone that we modified environment
        res = notify_setting_changed('Environment')
        print(f'System notification sent, return value: {res}')


    def dump_env_key(self, fname):
        if os_path.exists(fname):
            assert os_path.isfile(fname)
            try: os.remove(fname + '~')
            except Exception: pass
            os.rename(fname, fname + '~')
        subprocess.run('reg', 'export', f'HKLM\\{self.regkey}', fname, check=True)


    def normalize_paths(self, paths):
        '''I.e remove spaces, split on ';' and expand numeric references'''
        paths = chain.from_iterable(p.split(';') for p in paths)
        def f(path):
            path = path.strip()
            if path.isdigit():
                path = self.paths[int(path)]
            return path
        paths = [p for p in map(f, paths) if p]
        assert len(paths), 'No paths specified'
        return paths


    def remove_path(self, path):
        '''returns True if removed at least one instance'''
        newpaths = [p for p in self.paths if p != path]
        res = len(newpaths) != len(self.paths)
        self.paths = newpaths
        return res


    # commands
    @command(commands)
    def cmd_list(self):
        '''List current paths'''
        if self.paths:
            for i, s in enumerate(self.paths):
                print(f'{i:2d}: {s}')
        else:
            print('[Empty]')


    @command(commands)
    def cmd_add(self, *paths):
        '''Add paths to the front of the list (removing duplicates if exist)'''
        paths = reversed(self.normalize_paths(paths)) # so that add a b produces [a, b, ...]
        for path in paths:
            removed = self.remove_path(path)
            self.paths.insert(0, path)
            self.dirty = True
            print(f'''{path!r} {'moved' if removed else 'added'} to the front of the list''')


    @command(commands)
    def cmd_addback(self, *paths):
        '''Add paths to the end of the list (removing duplicates if exist)'''
        paths = self.normalize_paths(paths)
        for path in paths:
            removed = self.remove_path(path)
            self.paths.append(path)
            self.dirty = True
            print(f'''{path!r} {'moved' if removed else 'added'} to the back of the list''')


    @command(commands)
    def cmd_remove(self, *paths):
        '''Remove paths from the list'''
        paths = self.normalize_paths(paths)
        for path in paths:
            removed = self.remove_path(path)
            if removed:
                self.dirty = True
                print(f'{path!r} removed from the list')
            else:
                print(f'{path!r} not found in the list')


    @command(commands)
    def cmd_replace(self, *paths):
        '''Remove all paths and then add some'''
        paths = self.normalize_paths(paths)
        if len(self.paths) or len(paths): self.dirty = True
        self.paths = paths
        print('All paths removed from the list')
        for path in self.paths:
            print(f'{path!r} added to the list')


    @command(commands)
    def cmd_dump(self, fname = None):
        '''Dump the contents of the entire Environment registry key
        to 'environment.reg' or to the specified file, for backup'''
        if fname is None:
            fname = 'environment.reg'
        self.dump_env_key(fname)


    @command(commands)
    def cmd_help(self):
        '''Help on available commands'''
        desc = '''
        This program allows editing Windows systemwide PATH and other PATH-like
        environment variables containing semicolon-separated lists of values,
        like PATHEXT, INCLUDE, LIB etc. When editing other kinds of variables
        (with 'replace' command, probably) be aware that leading/trailing spaces,
        spaces around semicolons, empty or duplicate 'paths' etc can be removed,
        consider using the builtin 'reg' utility.

        These variables are located at
        'HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment'

        If the first argument is 'env', then the name of an environment variable
        to be edited should follow.
        If no more arguments are left, then the program starts in the interactive mode.
        Else remaining arguments should specify a valid command.
        '''
        print(doctrim(desc))
        print(' Basic commands:')
        for cmd in sorted(self.commands.keys()):
            print(' *', cmd, ':', doctrim(self.commands[cmd].__doc__))
        print(' Interactive commands:')
        for cmd in sorted(self.interactive_commands.keys()):
            if cmd not in self.commands:
                print(' *', cmd, ':',  doctrim(self.interactive_commands[cmd].__doc__))


    # interactive

    @command(interactive_commands)
    def cmd_abort(self):
        '''Abandon changes and quit'''
        self.dirty = False
        self.interactive = False


    @command(interactive_commands)
    def cmd_quit(self):
        '''Same as 'abort': abandon changes and quit'''
        self.cmd_abort()


    @command(interactive_commands)
    def cmd_reload(self):
        '''Abandon changes and reload value from the Registry.'''
        value = self.load_env_value()
        self.paths = [s for s in value.split(';') if s]
        self.dirty = False


    @command(interactive_commands)
    def cmd_commit(self):
        '''Commit changes to registry and quit'''
        self.interactive = False # will commit later

    interactive_commands.update(commands)


    def execute_command(self, commands, args):
        if len(args):
            try:
                cmd, args = args[0].lower(), args[1:]
                assert cmd in commands, (
                    'Unrecognized command: '' + cmd + '', expected one of '
                    + ', '.join(commands.keys()))
                commands[cmd](self, *args)
            except Exception as exc:
                traceback.print_exc()


    def execute(self, args):
        if len(args) >= 2 and args[0] == 'env':
            self.env_variable = args[1]
            args = args[2:]
        else:
            self.env_variable = 'PATH'

        self.cmd_reload()

        if len(args) == 0:
            self.interactive = True
            print('Interactive mode. Use "help" for help.')
            self.cmd_list()
            while self.interactive:
                args = parse_cmd(input('> '))
                self.execute_command(self.interactive_commands, args)
        else:
            self.execute_command(self.commands, args)

        if self.dirty:
            self.store_env_value(';'.join(self.paths))
            return 0
        return 1


def execute_command(args):
    return ExecutionScope().execute(args)


if __name__ == '__main__':
    sys.exit(execute_command(sys.argv[1:]))
