import os
import os.path as os_path
import filecmp

if os.name == 'nt':
	from installer_lib_win32utils import create_hardlink, get_file_id

class LinkedObject(object):
	"""
	Installation item representing a link from the installation target
	file/dir to file/dir in the repository. 
	
	It's always softlink on Linux.
	It's a hardlink for files and a junction for directories on Windows.
	
	The reason behind is that softlink always points to the repository and 
	if you move the repository somewhere else you'd probably have to reinstall
	anyway. Hardlink, on the other hand, points to the file system object
	that can easily become detached from the entry in the repository directory
	with no apparent manifestations.
	
	src is the name of the file/dir in the repository, not the source of the 
	link, by the way.
	
	Also it uses black magic to change its type in the constructor, to a 
	combination of (File/Dir, NT/Posix).
	
	Also it's targeted at XP/2k3 systems, should not forget to change the logic
	when moving to win7 to use softlinks for files.
	""" 

	def __init__(self, src, dst):
		self.src = os_path.abspath(src)
		self.dst = os_path.abspath(dst)
		self.is_file = os_path.isfile(self.src)
		
		assert os_path.exists(self.src), "Source object does not exists: " + self.src
		assert self.is_file or os_path.isdir(self.src), (
			"Source object is neither a file nor a directory: " + self.src)
		self.__class__ = self.classes[(os.name, self.is_file)]
		
	def destination_exists(self):
		return os_path.exists(self.dst)
	
	def already_installed(self):
		"""should be called only if destination_exists() returned true"""
		raise NotImplementedError()

	def can_be_removed(self):
		"""should be called only if destination_exists() returned true"""
		raise NotImplementedError()
	
	def remove_dest(self):
		raise NotImplementedError()
		
	def install(self):
		raise NotImplementedError()
	
class LinkedFileNT(LinkedObject):
	def already_installed(self):
		return get_file_id(self.src) == get_file_id(self.dst)
	def can_be_removed(self):
		return filecmp.cmp(self.src, self.dst, shallow = False)		
	def remove_dest(self):
		os.unlink(self.dst)
	def install(self):
		create_hardlink(self.dst, self.src) # remember, we have it reversed
	
	
class LinkedDirNT(LinkedObject):
	def already_installed(self):
		return get_file_id(self.src) == get_file_id(self.dst)
	def can_be_removed(self):
		return filecmp.cmp(self.src, self.dst, shallow = False)		
	def remove_dest(self):
		os.unlink(self.dst)
	def install(self):
		create_hardlink(self.dst, self.src) # remember, we have it reversed

class LinkedFilePosix(LinkedObject):
	pass
class LinkedDirPosix(LinkedObject):
	pass
	
LinkedObject.classes = {('nt', True) : LinkedFileNT,
			 			('nt', False) : LinkedDirNT,
					 	('posix', True) : LinkedFilePosix,
					 	('posix', False) : LinkedDirPosix}
