#!/usr/bin/env python
import sys
import os
import os.path
import string
import re
from os import popen
from optparse import OptionParser
from pprint import pprint

debug = False or True
def shellcmd(cmd, exit = True, read = True):
    if debug: print cmd
    proc = popen(cmd)
    if read:
        data = proc.read()
    exitcode = proc.close()
    if exitcode:
        exitcode = (exitcode & 0xFF00) >> 8 # it's multiplexed with signal
        print 'Shell command returned %d' % exitcode
        if data:
            print 'Also stdout contained this:\n%s\n' % data
        if exit:
            sys.exit(exitcode)
        return exitcode
    if read:
        return data
    return exitcode

def vimescape_unix(s):
    return re.sub(r'[^a-zA-Z0-9_/]', lambda m: '\\' + m.group(0), s)

def vimescape_win(s):
    return re.sub(r'[^a-zA-Z0-9_:.]', lambda m: '\\' + m.group(0), s)

def cmdescape_unix(s): 
    return re.sub(r'[^a-zA-Z0-9_/]', lambda m: '\\' + m.group(0), s)

def cmdescape_win(s): 
    return '"' + s.replace('"', '""') + '"'

if os.name == 'nt': 
    cmdescape = cmdescape_win
    vimescape = vimescape_win
else: 
    cmdescape = cmdescape_unix
    vimescape = vimescape_unix

basepath = shellcmd('hg root').strip()
# parse options
parser = OptionParser()
parser.add_option('-r', '--rev', dest='revisions', action='append', default=[])
parser.add_option('-I', '--include', dest='include', action='append', default=[])
parser.add_option('-X', '--exclude', dest='exclude', action='append', default=[])
parser.add_option('-d', '--debug', dest='debug', action="store_true", default=False) # this is mine, for debug printing

options, args = parser.parse_args()
if options.debug: debug = True # implication != equality

# compose options back, lol
def optionize(opt, values):
    def opt_one(value):
        if value: return '--' + opt + ' ' + cmdescape(value)
        return ''
    if values: return [opt_one(value) for value in values]
    return []
filecmd = ' '.join(
        optionize('include', options.include) + 
        optionize('exclude', options.exclude) +
        ['--'] + 
        [cmdescape(f) for f in args])

assert len(options.revisions) <= 2
options.revisions += [''] * (2 - len(options.revisions)) # pad with '' to 2 elements.
rev1, rev2 = optionize('rev', options.revisions)
rev1name, rev2name = ['[ revision ' + s + ' ] ' for s in options.revisions]
if not options.revisions[0]: rev1name = '[ parent ]'

files = shellcmd('hg status -mn ' + rev1 + ' ' + rev2 + ' ' + filecmd).split('\n')

for f in files:
    if f:
        f = os.path.join(basepath, f)
        vef = vimescape(f)

        # as of now vim supports up to ten commands, should be enough
        cmd = ['gvim -f ']
        def addcmd(s):
            cmd.append(' ' + cmdescape('+' + s))
        addcmd('set lazyredraw | e ' + rev1name) 
        addcmd('0r! hg cat ' + rev1 + ' ' + cmdescape(f))
        addcmd('diffthis | set buftype=nofile readonly')
        if rev2:
            addcmd('leftabove vert split ' + rev2name)
            addcmd('0r! hg cat ' + rev2 + ' ' + cmdescape(f))
            addcmd('diffthis | set buftype=nofile readonly')
        else:
            addcmd('leftabove vert diffsplit ' + vimescape(f))
        addcmd('syntax off | set nolazyredraw | redraw | normal gg') 

        cmd = ' '.join(cmd)

        if debug: print cmd

        exitcode = os.system(cmd)
        if exitcode:
            exitcode = (exitcode & 0xFF00) >> 8 # it's multiplexed with signal
            print 'Diff command returned %d' % exitcode
            sys.exit(exitcode)
