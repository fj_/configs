// ==UserScript==
// @name         IGM enhancements
// @namespace    http://your.homepage/
// @version      0.24
// @description  Idle Game Maker stuff, from http://forum.dashnet.org/discussion/4625/igm-bookmarklets-scripts/p1
// @author       You
// @match        http://orteil.dashnet.org/experiments/idlegamemaker/*
// @run-at       context-menu
// @grant        none
// ==/UserScript==

/*
Adds import/export buttons and a text box to the save box in IGM games.
Clicking the export button will display the save code in the text box.
Save the code to a text file or upload and then if you need to use the backup load this mod first then use the import button.
The functions created by the mod are modified versions of the save/load functions used by IGM.
*/

if(l('saveBox').innerHTML.indexOf("confirm('Are you sure you want to reset?')") >= 0){
    alert("IGM enhancements already applied, refresh if you want to apply again");
    return;
}

// Add a warning on reset button (also doubles as already applied detection).
l('saveBox').innerHTML = l('saveBox').innerHTML.replace("Game.Reset()","if(confirm('Are you sure you want to reset?')){Game.Reset()}");


// Import/export save game data
eval('Export=' + Game.Save.toString().replace('localStorage[Game.src]=str;','l(\'savecode\').value=str'));
eval('Import=' + Game.Load.toString().replace('function ()','function (str)').replace('var str=localStorage[Game.src];',''));

l('saveBox').innerHTML +="<div class='button' onclick='Export();'>Export Save</div><div class='button' onclick='Imports();'>Import Save</div><textarea id='savecode' style = 'resize: none'></textarea>";

Imports=function(){
    var savecode = prompt("Enter Save Code:","");
    if(savecode && savecode.length > 20){
        Import(savecode);
    }
}


// Add buy menu with multibuy options

buylots = function(amount)
{
    var build = l('multibuy').value;
    for(i=0; i<amount; i++)
    {
        if(Game.buildings[build].visible && Game.buildings[build].CanBuy())
        {
            Game.buildings[build].Buy();
        }
    }
}

populateMultibuyList = function()
{
    s = l('multibuy');
    for (var i = s.options.length - 1; i >= 0; i--) {
        s.remove(i);
    }
    for(i in Game.buildings) {
        if(Game.buildings[i].visible && Game.buildings[i].real) {
            var el = document.createElement("option");
            el.textContent = Game.buildings[i].name;
            el.value = Game.buildings[i].key;
            s.appendChild(el);
        }
    }
};

sellall = function()
{
    var build = l('multibuy').value;
    if(confirm('Are you sure you want to sell all ' + Game.buildings[build].plural + '?'))
    {
        while(Game.buildings[build].amount > 0 && Game.buildings[build].visible)
        {
            Game.buildings[build].Sell();
        }
    }
}

l('saveBox').innerHTML += ('<hr/>' +
        '<div class="button" onclick="populateMultibuyList()">refresh multibuy</div>' +
        '<select id="multibuy"></select>' +
        '<div class="button" onclick="buylots(10)">Buy 10</div>' +
        '<div class="button" onclick="buylots(25)">Buy 25</div>' +
        '<div class="button" onclick="buylots(100)">Buy 100</div>' +
        '<div class="button" onclick="sellall()">Sell all</div>');

populateMultibuyList();


// convert numbers to engineering notation
Beautify=function(num,floats)
{
    if (!isFinite(num)) return 'Infinity';
    var sign = num < 0 ? '-' : '';
    num = Math.abs(num);
    var i = 0;
    precision = Math.pow(10,5);
    num = Math.round(num * precision) / precision;
    if (num < 10000) return sign + num;
    while (Math.round(num) > 999)
    {
        num/=1000;
        i+=3;
    }
    num = num.toFixed(3).substring(0, 5);
    return sign + num + 'e' + i;
}
