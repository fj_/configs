// ==UserScript==
// @name         delete_fun_stuff
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://rdrama.net/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=rdrama.net
// @grant        GM_registerMenuCommand
// ==/UserScript==

GM_registerMenuCommand('Delete fun stuff', function() {
    'use strict';

	var stuff = document.querySelectorAll('#confetti-canvas, #post-award-confetti,' +
										  '#fog-effect-eco, #stab-effect-eco, #flashlight-effect-eco,' +
										  '.ricardo, .seal, .train, .bug, .firework');
	stuff.forEach(function(it) {
		it.remove();
	});

	stuff = document.querySelectorAll('.glow');
	console.log(stuff);
	stuff.forEach(function(it) {
		it.classList.remove('glow');
	});
}, 'd');
