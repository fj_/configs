// ==UserScript==
// @name         Reddit
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://*.reddit.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    function process(links) {
        links.forEach(function(link) {
            if (!link.href.endsWith('/')) {
                link.href += '/';
            }
            link.href += 'overview/';
        });
    }
    process(document.querySelectorAll('div.BlueBar > div.BlueBar__account > a'));
    process(document.querySelectorAll('#header-bottom-right > span.user > a'));
})();
