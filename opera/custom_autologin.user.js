// ==UserScript==
// @name         Custom autologin
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://vm:8443/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const username = 'csadmin';
    const password = '1';

    // CSP
    // const loginButtonXPath = '#submitBtn';
    // const alreadyLoggedInXPath = '.demo-info';
    const loginButtonXPath = '#loginButton';
    const alreadyLoggedInXPath = 'adasdasdasdaskgnrewkgner'; // not set

    var interval_id = null;

    function work() {
        if (document.querySelector(alreadyLoggedInXPath)) {
            console.log('Already logged in, clearing interval');
            clearInterval(interval_id);
            return;
        }

        var button = document.querySelector(loginButtonXPath);
        if (!button) return;
        console.log('Found login button, clearing interval:', button);

        document.querySelector('#username').value = username;
        document.querySelector('#password').value = password;
        button.click();
        clearInterval(interval_id);
    };

    interval_id = setInterval(work, 300);
    console.log('Set interval');
})();
