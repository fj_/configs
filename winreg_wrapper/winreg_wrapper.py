'''An object oriented wrapper for winreg module'''

import sys
import winreg
from winreg import (REG_SZ, REG_EXPAND_SZ, REG_DWORD, REG_BINARY,  # @UnusedImport
        HKEY_LOCAL_MACHINE, HKEY_CURRENT_USER, HKEY_CLASSES_ROOT)
import itertools

__all__ = ('REG_SZ', 'REG_EXPAND_SZ', 'REG_DWORD', 'REG_BINARY',
        'HKEY_LOCAL_MACHINE', 'HKEY_CURRENT_USER', 'HKEY_CLASSES_ROOT',
        'RegKey', 'get_python_bitness', 'get_os_bitness')


def get_python_bitness():
    '''Return 64 if current Python interpreter is 64bit, else 32.

    >>> get_python_bitness() in (32, 64)
    True
    '''
    return 64 if sys.maxsize > 2 ** 32 else 32


def get_os_bitness():
    '''Return 64 if current OS is 64bit, else 32.

    >>> get_os_bitness() in (32, 64)
    True
    '''
    # works incorrectly on Python2.6, by the way.
    import platform
    return {'x86': 32, 'AMD64': 64}[platform.machine()]


def split_reg_path(path):
    r'''Extract and parse the hive and remove empty components.

    >>> hive, path = split_reg_path('HKLM\\software\\\\Python\\\\')
    >>> hive == HKEY_LOCAL_MACHINE, path
    (True, 'software\\Python')

    '''
    str_to_hive = {
            'HKLM': HKEY_LOCAL_MACHINE,
            'HKEY_LOCAL_MACHINE': HKEY_LOCAL_MACHINE,

            'HKCU': HKEY_CURRENT_USER,
            'HKEY_CURRENT_USER': HKEY_CURRENT_USER,

            'HKCR': HKEY_CLASSES_ROOT,
            'HKEY_CLASSES_ROOT': HKEY_CLASSES_ROOT,
            }
    components = [s for s in path.split('\\') if s]
    assert len(components)
    return str_to_hive[components[0].upper()], '\\'.join(components[1:])

def fetch_tree(key):
    '''Fetch an entire registry tree rooted in key, as a pair of dictionaries
    {name: (value, type)}, {name: subkey}.

    The values in the subkey dictionary are the same pairs of dictionaries.
    '''
    keys, values = {}, {}
    for name in key.subkeys():
        with key.open(name) as subkey:
            keys[name] = fetch_tree(subkey)
    for name, value, type in key:
        values[name] = value, type
    return values, keys


class RegKey(object):
    '''Encapsulates an open registry key.

    Args:
        path: A string like r'HKCU\software'.
        *dummy: Must be empty, forces the following arguments to be keyword-only.
        write: If set to True, request write permission.
        create: If True, the key and all intermediate keys are created if don't exist. Implies
            `write`.
        allow_nonexistent: If True then no exception is raised if the key doesn't exist, `regkey.key`
            is None and context-manager-enter returns None (in `with RegKey(...) as key:`, key is None).
        bitness: 32 or 64 to force access to a particular registry view, None for native.
        parent: Optional parent `RegKey`, if specified then meaning of some of the arguments changes:
             `path`: must be relative.
             `write`: is inherited from the parent unless specified.
             `create`: is not inherited.
             `allow_nonexistent`: is not inherited.
             `bitness`: is inherited from parent, can't be specified.
             User code generally shouldn't specify `parent`, using `RegKey.open()` instance method instead.

    Values belonging to a key can be accessed similarly to a dictionary interface: `regkey[name]`
    and `regkey.get(name, [default])`, with a limitation that getting a value returns only the
    value, without the type. Pass with_type=True to get the type as well.

    Default (unnamed) value is accessed using an empty string. You can pass subkey_value=True to `get()`
    to get the default value of a subkey without having to open it first.

    Iterating over a `regkey` object iterates over values, yielding tuples (name, value, type).

    Subkeys can be accessed via `regkey.subkeys()` which iterates over subkey _names_ (without
    opening them), and `regkey.open()` that opens or creates a subkey.

    Do not add or remove values/subkeys while iterating over values/subkeys! Construct a list first
    like `for name in list(key.subkeys()):` if you need to.

    Context management protocol is supported.

    Note on `*dummy` and forcing users to use keyword-only arguments: in my experience it's
    extremely easy to mix up flags and worse bitness, and you wouldn't notice that anything is wrong
    on your dev machine.
    '''
    def __init__(self, path, *dummy, write=None, create=None, allow_nonexistent=None, bitness=None, parent=None):
        assert dummy is ()
        if allow_nonexistent and create:
            raise ValueError('Both `allow_nonexistent` and `create` are specified.')
        if create:
            if write is not None and not write:
                raise ValueError('`write=False` specified with `create`.')
        if parent is not None:
            self.path = parent.path + '\\' + path
            parent_key = parent.key
            self.write = parent.write if write is None else write
            self.bitness = parent.bitness
            assert bitness is None
        else:
            self.path = path
            parent_key, path = split_reg_path(path)
            self.write = write
            self.bitness = bitness
            assert bitness in (None, 32, 64)

        if self.write:
            access = winreg.KEY_ALL_ACCESS
        else:
            access = winreg.KEY_READ

        access |= self._get_wow_access()

        try:
            if create:
                self.key = winreg.CreateKeyEx(parent_key, path, 0, access)
            else:
                self.key = winreg.OpenKey(parent_key, path, 0, access)
        except WindowsError as exc:
            if exc.winerror == 2:
                if allow_nonexistent:
                    self.key = None
                else:
                    raise KeyError('Key {!r} doesn\'t exist'.format(self)) from None
            else:
                raise


    def _get_wow_access(self):
        return {None: 0, 32: winreg.KEY_WOW64_32KEY, 64: winreg.KEY_WOW64_64KEY}[self.bitness]


    def __repr__(self):
        return '{}({!r}, {}{})'.format(self.__class__, self.path, self.write,
                {None: '', 32: ', bitness=32', 64: ', bitness=64'}[self.bitness])


    # Sub-value manipulation

    def get(self, value_name, default=None, with_type=False, must_exist=False):
        '''Return the value for `value_name`, else `default`

        If `with_type` is true, return a tuple (value, type). If you provide `default`, it must be a
        two element tuple as well, otherwise (None, None) is returned.

        if must_exist=True then a KeyError is raised instead of returning default value, if value
        doesn't exist.
        '''
        if with_type and default is not None:
            value, type = default
        else:
            value, type = default, None

        try:
            value, type = winreg.QueryValueEx(self.key, value_name)
        except WindowsError as exc:
            if exc.winerror == 2:
                if must_exist:
                    raise KeyError('Value {!r} doesn\'t exist in {!r}'.format(value_name, self)) from None
            else:
                raise

        if with_type:
            return value, type
        return value


    def get_subkey_value(self, subkey_name, value_name='', default=None, with_type=False, must_exist=False):
        '''Utility function that opens `subkey` and returns its unnamed or named value.'''
        with self.open(subkey_name, allow_nonexistent=(not must_exist)) as k:
            if k is None:
                if with_type and default is None:
                    return None, None
                return default
            return k.get(value_name, default=default, with_type=with_type, must_exist=must_exist)


    def __getitem__(self, value_name):
        value = self.get(value_name)
        if value is None:
            raise KeyError('Value {!r} doesn\'t exist in {!r}'.format(value_name, self))
        return value


    def __setitem__(self, value_name, value):
        if isinstance(value, tuple):
            value, type = value
        else:
            if isinstance(value, int):
                type = REG_DWORD
            else:
                type = REG_SZ
                # ... and let SetValue figure it out
            # I could implement magic to check if Python type is str/unicode,
            # and there already exists a value, and its type is REG_EXPAND_SZ
            # or maybe even REG_BINARY, then I should use that type.
            # But I don't like magic.
        winreg.SetValueEx(self.key, value_name, 0, type, value)


    def del_value(self, value_name):
        winreg.DeleteValue(self.key, value_name)


    def __delitem__(self, value_name):
        self.del_value(value_name)


    def __contains__(self, value_name):
        return self.get(value_name) is not None


    def __iter__(self):
        for index in itertools.count():
            try:
                yield winreg.EnumValue(self.key, index)
            except WindowsError as exc:
                if exc.winerror == 259:
                    # no more data available
                    return
                raise


    def update(self, *args, **kwargs):
        '''Same as dict.update(), set values from the provided mapping or sequence and/or keyword arguments'''
        # carefully reproduce the exact dict.update() behavior (which is sort of weird).
        if len(args) == 1:
            other, = args
            if hasattr(other, 'keys'):
                for k in other.keys():
                    self[k] = other[k]
            else:
                for k, v in other:
                    self[k] = v
        else:
            raise TypeError('update expected at most 1 arguments, got {}'.format(len(args)))
        for k, v in kwargs.items():
            self[k] = v


    # subkey manipulation

    def subkeys(self):
        '''Iterate over subkey names. DO NOT ADD OR REMOVE KEYS WHILE ITERATING or construct a list first.'''
        for index in itertools.count():
            try:
                yield winreg.EnumKey(self.key, index)
            except WindowsError as exc:
                if exc.winerror == 259:
                    # no more data available
                    return
                raise


    def open(self, name, write=None, create=None, allow_nonexistent=None):
        '''Open a subkey. Same arguments as constructor, except parent is always set to this key.
        '''
        return RegKey(name, write=write, create=create,
                allow_nonexistent=allow_nonexistent, parent=self)


    def del_subkey(self, name):
        winreg.DeleteKeyEx(self.key, name, self._get_wow_access(), 0)

    def del_tree(self):
        '''delete this key and all subkeys'''
        for name in list(self.subkeys()):
            with self.open(name, write=True) as subkey:
                subkey.del_tree()
        # apparently it's OK. MSDN is silent, Wine specifically has it in tests.
        self.del_subkey('')

    # context manager

    def close(self):
        if self.key is not None:
            self.key.Close()
            # don't assign None so we get nice WindowsErrors when trying to access stuff after close.


    def __enter__(self):
        if self.key is not None:
            return self
        return None


    def __exit__(self, *exc_info):
        self.close()
        return False


if __name__ == '__main__':  # pragma: no cover
    def test():
        import doctest, nose
        from register_python_installation.tests import test_winreg_wrapper
        nose.main(doctest.DocTestSuite(), exit=False)
        nose.main(test_winreg_wrapper)
    test()
