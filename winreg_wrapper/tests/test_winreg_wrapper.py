import unittest, nose
import operator

if __name__ == "__main__" and __package__ is None:
    # hack to allow relative imports
    __package__ = 'winreg_wrapper.tests'
    import imp, sys
    imp.load_module('winreg_wrapper', None, sys.path[0] + '/..', 
            (None, None, imp.PKG_DIRECTORY))
    import winreg_wrapper.tests
    

from ..winreg_wrapper import (RegKey, fetch_tree, get_python_bitness,
        REG_SZ, REG_EXPAND_SZ, REG_BINARY, REG_DWORD)


def cleanup(path, initialize, bitness=None):
    with RegKey(path, create=True, write=True, bitness=bitness) as root:
        # open (or create), then destroy contents
        root.del_tree()
    if initialize:
        # recreate the contents
        with RegKey(path, create=True, bitness=bitness) as root:
            pass


class TestStuff(unittest.TestCase):
    # hopefully we don't mess up anything important
    path = r'hkcu\software\python\testing_playground'

    def setUp(self):
        cleanup(self.path, True)

    def test(self):
        with RegKey(self.path, write=True) as root:
            root[''] = 'default thing'
            root['asd'] = 'first string'
            root['def'] = 42
            root['fgh'] = ('Path is %PATH%', REG_EXPAND_SZ)
            with root.open('k2', create=True) as k2:
                k2[''] = 'subkey'
            with root.open('k2\\subkey2\\subkey3', create=True) as k3:
                k3['value'] = 'key'

            self.assertEqual(fetch_tree(root), (
                    {
                        '': ('default thing', REG_SZ),
                        'asd': ('first string', REG_SZ),
                        'fgh': ('Path is %PATH%', REG_EXPAND_SZ),
                        'def': (42, REG_DWORD),
                    },
                    {
                        'k2': (
                            {'': ('subkey', REG_SZ), },
                            {
                                'subkey2': ({}, {
                                     'subkey3': ({
                                         'value': ('key', REG_SZ)
                                         }, {})
                                 }),
                            }),
                    }))

            with root.open('k2\\subkey2\\subkey3') as k:
                k[''] = 'default'
                k['value'] = ('key', REG_EXPAND_SZ)
                self.assertTrue('value2' not in k)
                k['value2'] = (b'key\x00z', REG_BINARY)
                self.assertTrue('value2' in k)
                k['value3'] = 0xFFFFFF42
                self.assertEqual(fetch_tree(k), ({
                        '': ('default', REG_SZ),
                        'value': ('key', REG_EXPAND_SZ),
                        'value2': (b'key\x00z', REG_BINARY),
                        'value3': (0xFFFFFF42, REG_DWORD),
                        }, {}))
                k['value'] = '123'
                self.assertEqual(k['value'], '123')
                self.assertEqual(k.get('value', with_type=True), ('123', REG_SZ))
                del k['value']
                self.assertEqual(k.get('value'), None)
                self.assertEqual(k.get('value', with_type=True), (None, None))
                self.assertEqual(k.get('value', 1), 1)
                self.assertEqual(k.get('value', (1, 2), with_type=True), (1, 2))
                self.assertRaises(KeyError, lambda: k['value'])
                self.assertRaises(TypeError, lambda: k.get('value', 1, with_type=True))

            with root.open('k2') as k:
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3'), 'default')
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3', must_exist=True), 'default')
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3', 'value3', must_exist=True, with_type=True),
                        (0xFFFFFF42, REG_DWORD))

                self.assertEqual(k.get_subkey_value('subkey2\\subkey31', 'value3', with_type=True), (None, None))
                self.assertEqual(k.get_subkey_value('subkey2\\subkey31', 'value3', with_type=True, default=(1, 2)), (1, 2))
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3', 'value31', with_type=True), (None, None))
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3', 'value31', with_type=True, default=(1, 2)), (1, 2))
                self.assertEqual(k.get_subkey_value('subkey2\\subkey31', 'value3'), None)
                self.assertEqual(k.get_subkey_value('subkey2\\subkey31', 'value3', default=1), 1)
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3', 'value31'), None)
                self.assertEqual(k.get_subkey_value('subkey2\\subkey3', 'value31', default=1), 1)

                self.assertRaises(KeyError, lambda: k.get_subkey_value('subkey2\\subkey31', 'value3', with_type=True, must_exist=True))
                self.assertRaises(KeyError, lambda: k.get_subkey_value('subkey2\\subkey3', 'value31', default=1, must_exist=True))

            with root.open('k3', True, True) as k:
                self.assertRaises(TypeError, lambda: k.update({}, {}))
                k.update({'a': 1, 'b': ('2', REG_EXPAND_SZ)})
                k.update((('c', '3'), ('d', 4)))
                k.update({'e': 5}, other='6', g=('7', REG_SZ))
                self.assertEqual(fetch_tree(k), ({
                        'a': (1, REG_DWORD),
                        'b': ('2', REG_EXPAND_SZ),
                        'c': ('3', REG_SZ),
                        'd': (4, REG_DWORD),
                        'e': (5, REG_DWORD),
                        'other': ('6', REG_SZ),
                        'g': ('7', REG_SZ),
                        }, {}))

    def test_sanity(self):
        readonly_path = 'HKLM\\Software'
        with RegKey(readonly_path):
            pass

        self.assertRaises(WindowsError, lambda: RegKey(readonly_path, write=True))
        self.assertRaises(WindowsError, lambda: RegKey(readonly_path, write=True, create=True))
        self.assertRaises(WindowsError, lambda: RegKey(readonly_path, write=True, allow_nonexistent=True))

        # argument sanity check.
        self.assertRaises(ValueError, lambda: RegKey(readonly_path, write=True, create=True, allow_nonexistent=True))
        self.assertRaises(ValueError, lambda: RegKey(readonly_path, write=False, create=True))

        with RegKey(self.path, write=True) as root:
            root.close()
            root.close()
            # accessing closed key gives WindowsErrrors
            self.assertRaises(WindowsError, lambda: root['asd'])
            self.assertRaises(WindowsError, lambda: [x for x in root])
            self.assertRaises(WindowsError, lambda: [x for x in root.subkeys()])
        with RegKey(self.path) as root:
            self.assertRaises(KeyError, lambda: root.open('subkey'))

            with root.open('subkey', allow_nonexistent=True) as k:
                self.assertTrue(k is None)

            with root.open('subkey', create=True) as k:
                # succeeds because create=True, however we don't get write access
                self.assertRaises(WindowsError, lambda: operator.setitem(k, 'value', 'asd'))
                self.assertRaises(KeyError, lambda: k.open('subkey'))
            with root.open('subkey', create=True, write=True) as k:
                self.assertRaises(KeyError, lambda: k.open('subkey2'))
                k.open('subkey2', create=True).close()
                with k.open('subkey2') as k2:
                    k2['value'] = 'asd'
            self.assertEqual(fetch_tree(root), ({}, {
                    'subkey': ({}, {
                        'subkey2': ({'value': ('asd', REG_SZ)}, {})
                        })
                    }))

    def tearDown(self):
        cleanup(self.path, False)


class TestBitness(unittest.TestCase):
    # WOW64 redirects HKLM\software but that needs write acess for testing. This fits though
    path = r'hkcu\software\classes\clsid\{5CB7CC92-BD1A-4BD6-B219-AE114D0DB17F}\testing_playground'

    def setUp(self):
        cleanup(self.path, True, 32)
        cleanup(self.path, True, 64)


    def test(self):
        def create_structure(bitness):
            with RegKey(self.path, write=True, bitness=bitness) as root:
                root[''] = 'default thing'
                root['asd'] = 'first string'
                root['def'] = 42
                root['fgh'] = ('Path is %PATH%', REG_EXPAND_SZ)
                del root['def']
                with root.open('k2', create=True, write=True) as k2:
                    k2[''] = 'subkey'
                    k2.open('subkey_a', create=True).close()
                    k2.open('subkey_b', create=True).close()
                    k2.del_subkey('subkey_a')
                with root.open('k2\\subkey2\\subkey3', create=True) as k3:
                    k3['value'] = 'key'

        def check_structure(bitness):
            with RegKey(self.path, bitness=bitness) as root:
                self.assertEqual(fetch_tree(root), (
                        {
                            '': ('default thing', REG_SZ),
                            'asd': ('first string', REG_SZ),
                            'fgh': ('Path is %PATH%', REG_EXPAND_SZ),
                        },
                        {
                            'k2': (
                                {'': ('subkey', REG_SZ), },
                                {
                                    'subkey_b': ({}, {}),
                                    'subkey2': ({}, {
                                         'subkey3': ({
                                             'value': ('key', REG_SZ)
                                             }, {})
                                     }),
                                }),
                        }))
        native_bitness = get_python_bitness()

        create_structure(32)
        check_structure(32)
        with RegKey(self.path, bitness=64) as root:
            self.assertEqual(fetch_tree(root), ({}, {}))

        if 32 == native_bitness:
            check_structure(None)
        else:
            with RegKey(self.path) as root:
                self.assertEqual(fetch_tree(root), ({}, {}))

        create_structure(64)
        cleanup(self.path, True, 32)  # leave empty key

        check_structure(64)
        with RegKey(self.path, bitness=32) as root:
            self.assertEqual(fetch_tree(root), ({}, {}))

        if 64 == native_bitness:
            check_structure(None)
        else:
            with RegKey(self.path) as root:
                self.assertEqual(fetch_tree(root), ({}, {}))


    def tearDown(self):
        clsid_ends = self.path.find('}') + 1
        cleanup(self.path[:clsid_ends], False, 32)
        cleanup(self.path[:clsid_ends], False, 64)


if __name__ == '__main__':
    import winreg_wrapper.tests.test_winreg_wrapper
    nose.main(winreg_wrapper.tests.test_winreg_wrapper)

