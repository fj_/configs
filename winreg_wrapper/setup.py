#!/usr/bin/env python3

from setuptools import setup


setup(name='winreg_wrapper',
      version='0.1',
      description='Higher level, context manager enabled wrapper over winreg module.',
      author='Fj',
      author_email='fj.mail@gmail.com',
      license='MIT',
      py_modules=['winreg_wrapper'],
      test_suite='nose.collector',
      setup_requires=['nose', 'coverage'],
      tests_require=['nose', 'coverage'],
      zip_safe=True)
