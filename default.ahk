; use win-up/down to control system volume
SetBatchLines, 10ms

Hotkey, #x, vol_up
Hotkey, #z, vol_down

; end of the autoexecute section
return


; remap Win-F1 to Win-Ctrl-F1 and vice versa
$#F1::SendInput #^{F1}
$#^F1::SendInput #{F1}


; horisontal scrolling to tab switching
WheelLeft::^PgUp
WheelRight::^PgDn


; Vim and cygwin/x server (also for vim): remap <C-/> to <C-k><C-k>
#if WinActive("ahk_class Vim") or WinActive("ahk_exe XWin.exe")
   ^/::Send ^k^k
#ifWinActive


vol_up:
SendInput {Volume_Up}
return


vol_down:
SendInput {Volume_Down}
return
