#!/bin/sh
# Use full vim instead of the limited versions vim-minimal installs.
# Run as root.
# Grep is not anchored with ^ on purpose, since it can go to /usr/bin too.
rpm -ql vim-minimal | grep '/bin/' | xargs -p -I % ln -f -s /usr/bin/vim %
