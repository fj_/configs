import doctest, unittest
import installer_lib
from installer_lib import LinkedObject
import os
import shutil

test_dir_name = '!test_dir'

class TestLinkedObject(unittest.TestCase):
	@staticmethod
	def globalSetUp():
		shutil.rmtree(test_dir_name, True)
		os.mkdir(test_dir_name)
		os.chdir(test_dir_name)
		open('src0', 'w').write('1\n2\n')
		open('dst1', 'w').write('1\n2\n')
		open('dst2', 'w').write('1\n3\n')
		os.mkdir('srcdir')

	@staticmethod
	def globalTearDown():
		os.chdir('..')
		shutil.rmtree(test_dir_name)

	def test_source_not_exists(self):
		self.assertRaises(AssertionError, lambda: LinkedObject('not_exists', 'dst0'))

	def test_file_new(self):
		link = LinkedObject('src0', 'dst0')
		self.assert_(link.is_file)
		self.assert_(not link.destination_exists())
		link.install()
		self.assert_(link.destination_exists())
		self.assert_(link.already_installed())

	def test_file_equal(self):
		link = LinkedObject('src0', 'dst1')
		self.assert_(link.is_file)
		self.assert_(link.destination_exists())
		self.assert_(not link.already_installed())
		self.assert_(link.can_be_removed())
		link.remove_dest()
		link.install()
		self.assert_(link.already_installed())

	def test_file_different(self):
		link = LinkedObject('src0', 'dst2')
		self.assert_(link.is_file)
		self.assert_(link.destination_exists())
		self.assert_(not link.already_installed())
		self.assert_(not link.can_be_removed())

	def test_dir_new(self):
		link = LinkedObject('srcdir', 'dstdir')
		self.assert_(not link.is_file)
		self.assert_(not link.destination_exists())

class TestLoader2(unittest.TestLoader):
	"""Adds globalSetUp and globalTearDown hooks to TestCase.
	Both should be staticmethods (loader actually creates separate TestCases for 
	each test* method)."""
	def loadTestsFromTestCase(self, testCaseClass):
		"""Return a suite of all tests cases contained in testCaseClass"""
		suite = super(TestLoader2, self).loadTestsFromTestCase(testCaseClass)
		globalSetUp = getattr(testCaseClass, 'globalSetUp', lambda:None)
		globalTearDown = getattr(testCaseClass, 'globalTearDown', lambda:None)
		def wrap(f):
			def wrapper(*args, **kwargs):
				try:
					globalSetUp()
					f(*args, **kwargs)
				finally:
					globalTearDown()
			return wrapper
		suite.run = wrap(suite.run)
		suite.debug = wrap(suite.debug)
		return suite


if __name__ == '__main__':
	tests = (
			doctest.DocTestSuite(installer_lib),
			TestLoader2().loadTestsFromTestCase(TestLinkedObject),
			)
	r = unittest.TextTestRunner(verbosity=4)
	for test in tests:
		r.run(test)
	print "Yay"
