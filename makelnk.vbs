' is supposed to create links to files in the current directory (setting it 
' as working directory for the link as well), because I have no desire to
' actually learn vbscript =)

set shell = WScript.CreateObject("WScript.Shell")
set args = WScript.Arguments
if args.Count <> 2 then
    WScript.echo "wrong arguments count: " & args.Count
    WScript.quit -1
end if 
target = args(0)
source = args(1)

set link = shell.CreateShortcut(target)
link.TargetPath = shell.currentdirectory + "\" + source
link.WorkingDirectory = shell.currentdirectory
link.Save
